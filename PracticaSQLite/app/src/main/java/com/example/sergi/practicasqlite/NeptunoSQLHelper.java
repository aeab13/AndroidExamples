package com.example.sergi.practicasqlite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sergi on 14/03/2016.
 */
public class NeptunoSQLHelper extends SQLiteOpenHelper
{
    //region CONSTRUCTORS
    public NeptunoSQLHelper(Context context)
    {
        super(context, AppConstants.NOM_DB, null, AppConstants.VERSIO_DB);
    }

    public NeptunoSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    public NeptunoSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler)
    {
        super(context, name, factory, version, errorHandler);
    }
    //endregion

    //region EVENTS
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(AppConstants.sqlCreateProveedores);
        db.execSQL(AppConstants.sqlCreateCategorias);
        db.execSQL(AppConstants.sqlCreateProductos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
    //endregion
}
