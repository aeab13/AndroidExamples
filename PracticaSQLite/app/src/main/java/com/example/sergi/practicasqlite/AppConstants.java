package com.example.sergi.practicasqlite;

/**
 * Created by Sergi on 14/03/2016.
 */
public class AppConstants
{
    //region SQLITE
    public final static String NOM_DB = "BBDDNeptuno";
    public final static int VERSIO_DB = 1;
    public static final String COLUMNA_ID = "_id";//Primera columna per Android, SEMPRE _id!!!!

    //region COLUMNS PROVEEDORES
    public static final String TAULA_PROVEEDORES = "proveedores";
    public static final String COLUMNA_ID_PROVEEDOR = "id_proveedor";
    public static final String COLUMNA_NOMBRE_COMPANYIA = "companyia";
    public static final String COLUMNA_NOMBRE_CONTACTO = "contacto";
    public static final String COLUMNA_CARGO_CONTACTO = "cargo_contacto";
    public static final String COLUMNA_DIRECCION_PROVEEDOR = "direccion_proveedor";
    public static final String COLUMNA_CIUDAD_PROVEEDOR = "ciudad_proveedor";
    public static final String COLUMNA_REGION_PROVEEDOR = "region_proveedor";
    public static final String COLUMNA_POSTAL_PROVEEDOR = "postal_proveedor";
    public static final String COLUMNA_PAIS_PROVEEDOR = "pais_proveedor";
    public static final String COLUMNA_TELEFONO_PROVEEDOR = "telefono_proveedor";
    public static final String COLUMNA_FAX_PROVEEDOR = "fax_proveedor";
    public static final String COLUMNA_PAGINA_PROVEEDOR = "pagina_proveedor";
    //endregion

    //region COLUMNS PRODUCTOS
    public static final String TAULA_PRODUCTOS = "productos";
    public static final String COLUMNA_ID_PRODUCTO = "id_producto";
    public static final String COLUMNA_NOMBRE_PRODUCTO = "nombre";
    public static final String COLUMNA_CANTIDAD_UNIDAD = "cantidad_unidad";
    public static final String COLUMNA_PRECIO_UNIDAD = "precio_unidad";
    public static final String COLUMNA_UNI_EXIST = "unidad_exist";
    public static final String COLUMNA_UNI_PEDIDO = "unidad_pedido";
    public static final String COLUMNA_NIVEL_NUEVO_PEDIDO = "nivel_nuevo_pedido";
    public static final String COLUMNA_SUSPENDIDO = "suspendido";
    //endregion

    //region COLUMNS CATEGORIAS
    public static final String TAULA_CATEGORIAS = "categorias";
    public static final String COLUMNA_ID_CATEGORIA = "id_categoria";
    public static final String COLUMNA_NOMBRE_CATEGORIA = "nombre_categoria";
    public static final String COLUMNA_DESC_CATEGORIA = "desc_categoria";
    //endregion




    //region CREATE SENTENCES
    public static final  String sqlCreateProveedores = "CREATE TABLE " + AppConstants.TAULA_PROVEEDORES + " (" + AppConstants.COLUMNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AppConstants.COLUMNA_ID_PROVEEDOR + " TEXT NOT NULL, " +AppConstants.COLUMNA_NOMBRE_COMPANYIA +" TEXT NOT NULL, "  +AppConstants.COLUMNA_NOMBRE_CONTACTO +" TEXT NOT NULL, "
            +AppConstants.COLUMNA_CARGO_CONTACTO +" TEXT NOT NULL, " +AppConstants.COLUMNA_DIRECCION_PROVEEDOR +" TEXT NOT NULL, " +AppConstants.COLUMNA_CIUDAD_PROVEEDOR +" TEXT NOT NULL, "
            +AppConstants.COLUMNA_REGION_PROVEEDOR +" TEXT NOT NULL, " +AppConstants.COLUMNA_POSTAL_PROVEEDOR +" TEXT NOT NULL, " +AppConstants.COLUMNA_PAIS_PROVEEDOR +" TEXT NOT NULL, "
            +AppConstants.COLUMNA_TELEFONO_PROVEEDOR +" TEXT NOT NULL, " +AppConstants.COLUMNA_FAX_PROVEEDOR +" TEXT NOT NULL, " +AppConstants.COLUMNA_PAGINA_PROVEEDOR +" TEXT NOT NULL);";

    public static final  String sqlCreateProductos = "CREATE TABLE " + AppConstants.TAULA_PRODUCTOS + " (" + AppConstants.COLUMNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AppConstants.COLUMNA_ID_PRODUCTO + " TEXT NOT NULL, " +AppConstants.COLUMNA_NOMBRE_PRODUCTO +" TEXT NOT NULL, "  +AppConstants.COLUMNA_ID_PROVEEDOR +" TEXT NOT NULL, "
            +AppConstants.COLUMNA_ID_CATEGORIA +" TEXT NOT NULL, " +AppConstants.COLUMNA_CANTIDAD_UNIDAD +" TEXT NOT NULL, " +AppConstants.COLUMNA_PRECIO_UNIDAD +" TEXT NOT NULL, "
            +AppConstants.COLUMNA_UNI_EXIST +" TEXT NOT NULL, " +AppConstants.COLUMNA_UNI_PEDIDO +" TEXT NOT NULL, " +AppConstants.COLUMNA_NIVEL_NUEVO_PEDIDO +" TEXT NOT NULL, "
            +AppConstants.COLUMNA_SUSPENDIDO +" TEXT NOT NULL);";

    public static final  String sqlCreateCategorias = "CREATE TABLE " + AppConstants.TAULA_CATEGORIAS + " (" + AppConstants.COLUMNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AppConstants.COLUMNA_ID_CATEGORIA + " TEXT NOT NULL, " +AppConstants.COLUMNA_NOMBRE_CATEGORIA + " TEXT NOT NULL, " +AppConstants.COLUMNA_DESC_CATEGORIA + " TEXT NOT NULL);";

    //endregion

    //region DELETE SENTENCES
    public final static String DELETE_PROVEEDORES = "DELETE FROM "+ AppConstants.TAULA_PROVEEDORES;

    public final static String DELETE_PRODUCTOS = "DELETE FROM "+ AppConstants.TAULA_PRODUCTOS;

    public final static String DELETE_CATEGORIAS = "DELETE FROM "+ AppConstants.TAULA_CATEGORIAS;

    //endregion

    //endregion
}
