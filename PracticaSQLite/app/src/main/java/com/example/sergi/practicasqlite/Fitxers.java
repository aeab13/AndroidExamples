package com.example.sergi.practicasqlite;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import Classes.Categorias;
import Classes.Productos;
import Classes.Proveedores;

/**
 * Created by Xevi on 15/03/2015.
 */
public class Fitxers {

    public static ArrayList<Categorias> obtenerCategorias(Context contexte) {
        ArrayList<Categorias> categorias = new ArrayList<>();

        try {
            String linia;
            String[] camps;
            String idCat = "";
            String nombreCat = "";
            String descCat = "";

            InputStream fitxerRaw =  contexte.getResources().openRawResource(R.raw.categorias);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            brEntrada.readLine();//ELIMINAR LA CAPÇALERA

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                try {
                    idCat = "";
                    if (camps[0] != null)
                        idCat = camps[0];
                }
                catch (Exception ex){}
                try {
                    nombreCat = "";
                    if (camps[1] != null)
                        nombreCat = camps[1];
                }
                catch (Exception ex){}
                try {
                    descCat = "";
                    if (camps[2] != null)
                        descCat = camps[2];
                }
                catch (Exception ex){}

                categorias.add(new Categorias(idCat, nombreCat, descCat));
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.i("ErroProv",ex.getMessage());
        }

        return categorias;
    }

    public static ArrayList<Proveedores> obtenerProveedores(Context contexte) {
        ArrayList<Proveedores> proveedores = new ArrayList<Proveedores>();

        try {
            String linia;
            String[] camps;
            String codigo = "";
            String nombreComp = "";
            String nombreContacto = "";
            String cargo = "";
            String direccion = "";
            String ciudad = "";
            String region = "";
            String postal = "";
            String pais = "";
            String telefono = "";
            String fax = "";
            String web = "";

            InputStream fitxerRaw =  contexte.getResources().openRawResource(R.raw.proveedores);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            brEntrada.readLine();//ELIMINAR LA CAPÇALERA

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                try {
                    codigo = "";
                    if (camps[0] != null)
                        codigo = camps[0];
                }
                catch (Exception ex){}
                try {
                    nombreComp = "";
                    if (camps[1] != null)
                        nombreComp = camps[1];
                }
                catch (Exception ex){}
                try {
                    nombreContacto = "";
                    if (camps[2] != null)
                        nombreContacto = camps[2];
                }
                catch (Exception ex){}
                try {
                    cargo = "";
                    if (camps[3] != null)
                        cargo = camps[3];
                }
                catch (Exception ex){}
                try {
                    direccion = "";
                    if (camps[4] != null)
                        direccion = camps[4];
                }
                catch (Exception ex){}
                try {
                    ciudad = "";
                    if (camps[5] != null)
                        ciudad = camps[5];
                }
                catch (Exception ex){}
                try {
                    region = "";
                    if (camps[6] != null)
                        region = camps[6];
                }
                catch (Exception ex){}
                try {
                    postal = "";
                    if (camps[7] != null)
                        postal = camps[7];
                }
                catch (Exception ex){}
                try {
                    pais = "";
                    if (camps[8] != null)
                        pais = camps[8];
                }
                catch (Exception ex){}
                try {
                    telefono = "";
                    if (camps[9] != null)
                        telefono = camps[9];
                }
                catch (Exception ex){}
                try {
                    fax = "";
                    if (camps[10] != null)
                        fax = camps[10];
                }
                catch (Exception ex){}
                try {
                    web = "";
                    if (camps[11] != null)
                        web = camps[11];
                }
                catch (Exception ex){}

                proveedores.add(new Proveedores(codigo, nombreComp, nombreContacto, cargo, direccion, ciudad, region, postal,
                        pais, telefono, fax, web));
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.i("ErroProv",ex.getMessage());
        }

        return proveedores;
    }

    public static ArrayList<Productos> obtenerProductos(Context contexte) {
        ArrayList<Productos> productos = new ArrayList<Productos>();

        try {
            String linia;
            String[] camps;
            String idProducto = "";
            String nombreProducto = "";
            String idProveedor = "";
            String idCategoria = "";
            String cantidadUnidad = "";
            String precioUnidad = "";
            String uniExist = "";
            String uniPedido = "";
            String nivelNuevoPedido = "";
            String suspendido = "";

            InputStream fitxerRaw =  contexte.getResources().openRawResource(R.raw.productos);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            brEntrada.readLine();//ELIMINAR LA CAPÇALERA

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                try {
                    idProducto = "";
                    if (camps[0] != null)
                        idProducto = camps[0];
                }
                catch (Exception ex){}
                try {
                    nombreProducto = "";
                    if (camps[1] != null)
                        nombreProducto = camps[1];
                }
                catch (Exception ex){}
                try {
                    idProveedor = "";
                    if (camps[2] != null)
                        idProveedor = camps[2];
                }
                catch (Exception ex){}
                try {
                    idCategoria = "";
                    if (camps[3] != null)
                        idCategoria = camps[3];
                }
                catch (Exception ex){}
                try {
                    cantidadUnidad = "";
                    if (camps[4] != null)
                        cantidadUnidad = camps[4];
                }
                catch (Exception ex){}
                try {
                    precioUnidad = "";
                    if (camps[5] != null)
                        precioUnidad = camps[5];
                }
                catch (Exception ex){}
                try {
                    uniExist = "";
                    if (camps[6] != null)
                        uniExist = camps[6];
                }
                catch (Exception ex){}
                try {
                    uniPedido = "";
                    if (camps[7] != null)
                        uniPedido = camps[7];
                }
                catch (Exception ex){}
                try {
                    nivelNuevoPedido = "";
                    if (camps[8] != null)
                        nivelNuevoPedido = camps[8];
                }
                catch (Exception ex){}
                try {
                    suspendido = "";
                    if (camps[9] != null)
                        suspendido = camps[9];
                }
                catch (Exception ex){}

                productos.add(new Productos(idProducto, nombreProducto, idProveedor, idCategoria, cantidadUnidad, precioUnidad, uniExist, uniPedido,
                        nivelNuevoPedido, suspendido));
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.i("ErroProv",ex.getMessage());
        }

        return productos;
    }


}