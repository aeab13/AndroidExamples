package Classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SergiDAM on 29/03/2016.
 */
public class Categorias implements Parcelable {

    private String idCategoria;
    private String nombreCategoria;
    private String descripcionCategoria;

    public Categorias(){}

    public Categorias(String idCategoria, String nombreCategoria, String descripcionCategoria) {
        this.idCategoria = idCategoria;
        this.descripcionCategoria = descripcionCategoria;
        this.nombreCategoria = nombreCategoria;
    }

    protected Categorias(Parcel in) {
        this.idCategoria = in.readString();
        this.nombreCategoria = in.readString();
        this.descripcionCategoria = in.readString();
    }

    //region PROPERTIES
    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }

    //endregion

    @Override
    public String toString(){
        return nombreCategoria;
    }


    //region PARCELABLE
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idCategoria);
        dest.writeString(nombreCategoria);
        dest.writeString(descripcionCategoria);
    }
    public void readFromParcel(Parcel dades){
        idCategoria = dades.readString();
        nombreCategoria = dades.readString();
        descripcionCategoria = dades.readString();
    }

    public static final Creator<Categorias> CREATOR = new Creator<Categorias>() {
        @Override
        public Categorias createFromParcel(Parcel in) {
            return new Categorias(in);
        }

        @Override
        public Categorias[] newArray(int size) {
            return new Categorias[size];
        }
    };
    //endregion
}
