package Classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sergi on 14/03/2016.
 */

public class Proveedores implements Parcelable{
    private String codigoProveedores;
    private String nombreCompañia;
    private String nombreContacto;
    private String cargo;
    private String direccion;
    private String ciudad;
    private String region;
    private String codigoPostal;
    private String pais;
    private String telefono;
    private String fax;
    private String web;

    public Proveedores(){}

    public Proveedores(String codigoProveedores, String nombreCompañia, String nombreContacto, String cargo,
                       String direccion, String ciudad, String region, String codigoPostal, String pais,
                       String telefono, String fax, String web)
    {
        this.codigoProveedores = codigoProveedores;
        this.nombreCompañia = nombreCompañia;
        this.nombreContacto = nombreContacto;
        this.cargo = cargo;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.region = region;
        this.codigoPostal = codigoPostal;
        this.pais = pais;
        this.telefono = telefono;
        this.fax = fax;
        this.web = web;
    }

    protected Proveedores(Parcel in) {
        this.codigoProveedores = in.readString();
        this.nombreCompañia = in.readString();
        this.nombreContacto = in.readString();
        this.cargo = in.readString();
        this.direccion = in.readString();
        this.ciudad = in.readString();
        this.region = in.readString();
        this.codigoPostal = in.readString();
        this.pais = in.readString();
        this.telefono = in.readString();
        this.fax = in.readString();
        this.web = in.readString();
    }

    //region PROPERTIES
    public String getCodigoProveedores() {
        return codigoProveedores;
    }

    public void setCodigoProveedores(String codigoProveedores) {
        this.codigoProveedores = codigoProveedores;
    }

    public String getNombreCompañia() {
        return nombreCompañia;
    }

    public void setNombreCompañia(String nombreCompañia) {
        this.nombreCompañia = nombreCompañia;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    //endregion

    @Override
    public String toString(){
        return nombreCompañia;
    }


    //region PARCELABLE
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(codigoProveedores);
        dest.writeString(nombreCompañia);
        dest.writeString(nombreContacto);
        dest.writeString(cargo);
        dest.writeString(direccion);
        dest.writeString(ciudad);
        dest.writeString(region);
        dest.writeString(codigoPostal);
        dest.writeString(pais);
        dest.writeString(telefono);
        dest.writeString(fax);
        dest.writeString(web);
    }
    public void readFromParcel(Parcel dades){
        codigoProveedores = dades.readString();
        nombreCompañia = dades.readString();
        nombreContacto = dades.readString();
        cargo = dades.readString();
        direccion = dades.readString();
        ciudad = dades.readString();
        region = dades.readString();
        codigoPostal = dades.readString();
        pais = dades.readString();
        telefono = dades.readString();
        fax = dades.readString();
        web = dades.readString();
    }

    public static final Creator<Proveedores> CREATOR = new Creator<Proveedores>() {
        @Override
        public Proveedores createFromParcel(Parcel in) {
            return new Proveedores(in);
        }

        @Override
        public Proveedores[] newArray(int size) {
            return new Proveedores[size];
        }
    };
    //endregion
}
