package cat.duba.accediralesdades.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import cat.duba.accediralesdades.Adapters.AdaptadorCategories;
import cat.duba.accediralesdades.Contracts.CategoriesContract;
import cat.duba.accediralesdades.ProductesPerCategoria;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 28/03/16.
 */
public class CategoriesMestre extends Fragment {
    ListView lvCategories;
    public static final String IDCATEGORIA="id_categoria";
    Cursor cursorCategories;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.categories_mestre, container, false);

        lvCategories= (ListView) view.findViewById(R.id.lvCategories);
        cursorCategories=getActivity().getContentResolver().query(CategoriesContract.CONTENT_URI, null, null, null, null);

        lvCategories.setAdapter(new AdaptadorCategories(getActivity(), cursorCategories, 0));
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String categoriaSeleccionada;
                AdaptadorCategories adaptador = (AdaptadorCategories) lvCategories.getAdapter();
                Cursor cursorSpinner = adaptador.getCursor();
                categoriaSeleccionada = cursorSpinner.getString(1);
                Intent intent=new Intent(getActivity(), ProductesPerCategoria.class);
                intent.putExtra(IDCATEGORIA, categoriaSeleccionada);
                startActivity(intent);
            }
        });
        return view;
    }

}
