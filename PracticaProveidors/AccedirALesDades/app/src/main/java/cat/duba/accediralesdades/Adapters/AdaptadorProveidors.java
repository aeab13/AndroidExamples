package cat.duba.accediralesdades.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import cat.duba.accediralesdades.Contracts.ProveidorsContract;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 6/04/16.
 */
public class AdaptadorProveidors extends CursorAdapter {
    private LayoutInflater cursorInflater;

    public AdaptadorProveidors(Context context, Cursor c) {
        super(context, c);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorProveidors(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorProveidors(Context context, Cursor c, int flags) {
        super(context, c, flags);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.proveedores_view_adapter,parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvIdProveedor=(TextView)view.findViewById(R.id.tvIdProveedor);
        TextView tvNombreCompania=(TextView)view.findViewById(R.id.tvNombreCompania);


        tvIdProveedor.setText(cursor.getString(cursor.getColumnIndex(ProveidorsContract.Columnes.PROVEEDORES_ID_PROVEEDOR)));
        if(cursor.getColumnIndex(ProveidorsContract.Columnes.PROVEEDORES_NOMBRE_COMPANIA)!=-1)
        tvNombreCompania.setText(cursor.getString(cursor.getColumnIndex(ProveidorsContract.Columnes.PROVEEDORES_NOMBRE_COMPANIA)));
    }
}