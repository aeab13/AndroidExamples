package cat.duba.accediralesdades;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import cat.duba.accediralesdades.Adapters.AdaptadorProductes;
import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.Fragments.CategoriesMestre;

public class ProductesPerCategoria extends AppCompatActivity {
    private ListView lvProductes;
    private Cursor cursorProductes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productes_categoria);
        Intent intent=getIntent();
        lvProductes= (ListView) findViewById(R.id.lvProductes);
        String categoriaSeleccionada;
        categoriaSeleccionada = intent.getStringExtra(CategoriesMestre.IDCATEGORIA);
        cursorProductes=getContentResolver().query(ProductesContract.CONTENT_URI, null, ProductesContract.Columnes.PRODUCTOS_ID_CATEGORIA+ " = " + categoriaSeleccionada, null, null);
        lvProductes.setAdapter(new AdaptadorProductes(this,cursorProductes));
    }
}
