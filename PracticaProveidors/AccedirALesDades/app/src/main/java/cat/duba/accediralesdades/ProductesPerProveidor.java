package cat.duba.accediralesdades;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import cat.duba.accediralesdades.Adapters.AdaptadorProductes;
import cat.duba.accediralesdades.Contracts.ProductesContract;

/**
 * Created by abde on 7/04/16.
 */
public class ProductesPerProveidor extends AppCompatActivity {
    private ListView lvProductes;
    private Cursor cursorProductes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productes_categoria);
        Intent intent=getIntent();
        lvProductes= (ListView) findViewById(R.id.lvProductes);
        String proveedorSeleccionado;
        proveedorSeleccionado = intent.getStringExtra(Proveidors.IDPROVEIDOR);
        cursorProductes=getContentResolver().query(ProductesContract.CONTENT_URI, null, ProductesContract.Columnes.PRODUCTOS_ID_PROVEEDOR+ " = " + proveedorSeleccionado, null, null);
        lvProductes.setAdapter(new AdaptadorProductes(this,cursorProductes));
    }
}
