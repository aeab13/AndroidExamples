package cat.duba.accediralesdades.Fragments;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import cat.duba.accediralesdades.Adapters.AdaptadorCategories;
import cat.duba.accediralesdades.Adapters.AdaptadorProductes;
import cat.duba.accediralesdades.Contracts.CategoriesContract;
import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 28/03/16.
 */
public class CategoriesDetall extends Fragment {
    ListView lvCategories, lvProductes;
    Cursor cursorCategories, cursorProductes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.categories_detall, container, false);
        lvCategories= (ListView) view.findViewById(R.id.lvCategories);
        lvProductes= (ListView) view.findViewById(R.id.lvProductes);
        cursorCategories=getActivity().getContentResolver().query(CategoriesContract.CONTENT_URI, null, null, null, null);

        //lvProductes= (ListView) view.findViewById(R.id.lvProductes);
        lvCategories.setAdapter(new AdaptadorCategories(getActivity(), cursorCategories, 0));
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                carregaProductes();
            }
        });
/*        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductosDAO productosDAO=new ProductosDAO(getActivity());
                productosDAO.obre();
                lvProductes.setAdapter(new AdaptadorProductes(productosDAO.obteProductosAmbCategoria((int)lvCategories.getItemIdAtPosition(position)), getActivity()));
            }
        });*/
        return view;
    }
    private void carregaProductes()
    {
        String categoriaSeleccionada;
        AdaptadorCategories adaptador = (AdaptadorCategories) lvCategories.getAdapter();
        Cursor cursorSpinner = adaptador.getCursor();
        categoriaSeleccionada = cursorSpinner.getString(1);
        cursorProductes=getActivity().getContentResolver().query(ProductesContract.CONTENT_URI, null, ProductesContract.Columnes.PRODUCTOS_ID_CATEGORIA+ " = " + categoriaSeleccionada, null, null);
        lvProductes.setAdapter(new AdaptadorProductes(getActivity(),cursorProductes));
    }
}
