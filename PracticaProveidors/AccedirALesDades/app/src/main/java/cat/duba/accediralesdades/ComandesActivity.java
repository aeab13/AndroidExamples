package cat.duba.accediralesdades;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import cat.duba.accediralesdades.Adapters.AdaptadorPedidos;
import cat.duba.accediralesdades.Adapters.AdaptadorProveidors;
import cat.duba.accediralesdades.Contracts.ComandesContract;
import cat.duba.accediralesdades.Contracts.DetallesPedidoContract;
import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.ModificacionsNous.NouPedido;

public class ComandesActivity extends AppCompatActivity {
    ListView lvPedidos;
    Cursor cursorPedidos, cursorDetalles;
    public static String IDPEDIDO="idPedido", CARGO="cargo", MODIFICARPEDIDO="pedidoAModificar", ESMODIFICAT="EsModificat";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comandes);

        lvPedidos= (ListView) findViewById(R.id.lvPedidos);
        cursorPedidos=getContentResolver().query(ComandesContract.CONTENT_URI, null, null, null, null);

        lvPedidos.setAdapter(new AdaptadorPedidos(this, cursorPedidos));
        lvPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pedidoSeleccionado;
                AdaptadorPedidos adaptador = (AdaptadorPedidos) lvPedidos.getAdapter();
                Cursor cursorSpinner = adaptador.getCursor();
                pedidoSeleccionado = cursorSpinner.getString(1);
                Intent intent=new Intent(ComandesActivity.this, DetallesPorPedido.class);
                intent.putExtra(IDPEDIDO, pedidoSeleccionado);
                startActivity(intent);

            }
        });
        lvPedidos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                String[] opcions = new String[]{"Eliminar"};
                ListView llista = new ListView(ComandesActivity.this);
                llista.setAdapter(new ArrayAdapter<String>(ComandesActivity.this, android.R.layout.simple_list_item_1, opcions));
                final AlertDialog dialog = (new AlertDialog.Builder(ComandesActivity.this)
                        .setTitle("Elige una opción")
                        .setView(llista)
                        .setCancelable(true)
                        .create());
                dialog.show();
                llista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int positionB, long id) {
                        switch (positionB) {
                            case 0:
                                AdaptadorPedidos adaptador = (AdaptadorPedidos) lvPedidos.getAdapter();
                                Cursor cursorSpinner = adaptador.getCursor();
                                cursorDetalles = ComandesActivity.this.getContentResolver().query(DetallesPedidoContract.CONTENT_URI, null, DetallesPedidoContract.Columnes.DETALLES_ID_PEDIDO+ " = " + cursorSpinner.getString(1), null, null);
                                int depenen = cursorDetalles.getCount();// proveedoresDAO.EnDepenen((int) lvProveedores.getItemIdAtPosition(position));



                                if (depenen == 0) {
                                    Toast.makeText(ComandesActivity.this, "Pedido eliminado", Toast.LENGTH_LONG).show();
                                    lvPedidos.setAdapter(new AdaptadorPedidos(ComandesActivity.this, cursorPedidos));
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(ComandesActivity.this, "No se puede eliminar este pedido " + depenen, Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                                break;
                        }
                    }
                });

                return false;
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_proveidors, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuAlta:
                startActivity(new Intent(getBaseContext(), NouPedido.class));
                return true;
            default:
                break;
        }

        return false;
    }
}

