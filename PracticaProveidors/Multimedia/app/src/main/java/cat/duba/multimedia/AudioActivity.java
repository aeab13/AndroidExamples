package cat.duba.multimedia;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AudioActivity extends AppCompatActivity implements View.OnClickListener {

    SoundPool soundPool;
    int idAudio;
    boolean carregat=false;
    boolean reproduint=false;
    int comptador=0;
    float volumActual, volumMaxim,volum;
    Button btnReprodueix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        btnReprodueix= (Button) findViewById(R.id.btnReproduir);
        if (btnReprodueix != null) {
            btnReprodueix.setOnClickListener(this);
        }
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        creaSoundPool();
    }

    private void creaSoundPool() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){

            AudioAttributes audioAttributes=new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_MEDIA).setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build();

            soundPool=new SoundPool.Builder().setMaxStreams(25).setAudioAttributes(audioAttributes).build();

            idAudio=soundPool.load(this, R.raw.tuturu_century_fox, comptador);

            carregat=true;
        }
        else
        {
            AudioManager audioManager= (AudioManager) getSystemService(AUDIO_SERVICE);
            volumActual=audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            volumMaxim=audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            volum=volumActual/volumMaxim;
            soundPool=new SoundPool(25,AudioManager.STREAM_MUSIC, 0);
            soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    carregat=true;
                }
            });
            idAudio=soundPool.load(this, R.raw.simple_tuturu, comptador);
        }
    }

    public void reprodueixAudio()
    {
        if(carregat&&!reproduint)
        {
            soundPool.play(idAudio, 100, 100, comptador, 0, 1f);
            reproduint=true;
        }
    }

    public void aturaAudio()
    {

        if(reproduint)
            soundPool.stop(idAudio);

    }

    @Override
    public void onClick(View v) {
        if(reproduint)
            aturaAudio();
        else
            reprodueixAudio();
    }
}
