package proveidor_dades.Categoria;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by abde on 4/04/16.
 */
public class CategoriesContract {
    private CategoriesContract()
    {}

    public static final String ESQUEMA = "content://";

    public static final String AUTHORITY = "proveidor_dades.Categoria";

    public static final String BASE_PATH = "categorias";

    public static final Uri CONTENT_URI = Uri.parse(ESQUEMA+AUTHORITY+"/"+BASE_PATH);

    public static final int TOTES_LES_FILES = 1;

    public static final int UNA_FILA = 2;

    public static final UriMatcher sUriMatcher;
    static
    {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, TOTES_LES_FILES);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH+"/#", UNA_FILA);
    }

    public final static class Columnes implements BaseColumns
    {
        public static final String CATEGORIAS_ID_CATEGORIA = Constants.CATEGORIAS_ID_CATEGORIA;
        public static final String CATEGORIAS_NOMBRE_CATEGORIA= Constants.CATEGORIAS_NOMBRE_CATEGORIA;
        public static final String CATEGORIAS_DESCRIPCION= Constants.CATEGORIAS_DESCRIPCION;
        public static final String CATEGORIAS_IMAGEN=Constants.CATEGORIAS_IMAGEN;
    }

    public final static String MIME_UNA = "vnd.android.cursos.item/vnd." +AUTHORITY+"/"+BASE_PATH;

    public final static String MIME_MULTIPLE = "vnd.android.cursos.dir/vnd." +AUTHORITY+"/"+BASE_PATH;
}
