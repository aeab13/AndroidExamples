package proveidor_dades.Producte;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by abde on 5/04/16.
 */
public class ProductesContract {
    private ProductesContract()
    {}

    public static final String ESQUEMA = "content://";

    public static final String AUTHORITY = "proveidor_dades.Producte";

    public static final String BASE_PATH = "productos";

    public static final Uri CONTENT_URI = Uri.parse(ESQUEMA+AUTHORITY+"/"+BASE_PATH);

    public static final int TOTES_LES_FILES = 1;

    public static final int UNA_FILA = 2;

    public static final UriMatcher sUriMatcher;
    static
    {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, TOTES_LES_FILES);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH+"/#", UNA_FILA);
    }

    public final static class Columnes implements BaseColumns
    {
        public static final String
                PRODUCTOS_ID_PRODUCTO="id_producto",
                PRODUCTOS_NOMBRE_PRODUCTO="nombre_producto",
                PRODUCTOS_ID_PROVEEDOR="id_proveedor",
                PRODUCTOS_ID_CATEGORIA="id_categoria",
                PRODUCTOS_CANTIDAD_POR_UNIDAD="cantidad_por_unidad",
                PRODUCTOS_PRECIO_UNIDAD="precio_unidad",
                PRODUCTOS_UNIDADES_EN_EXISTENCIA="unidades_en_existencia",
                PRODUCTOS_UNIDADES_EN_PEDIDO="unidades_en_pedido",
                PRODUCTOS_NIVEL_NUEVO_PEDIDO="nivel_nuevo_pedido",
                PRODUCTOS_SUSPENDIDO="suspendido";
    }

    public final static String MIME_UNA = "vnd.android.cursos.item/vnd." +AUTHORITY+"/"+BASE_PATH;

    public final static String MIME_MULTIPLE = "vnd.android.cursos.dir/vnd." +AUTHORITY+"/"+BASE_PATH;
}