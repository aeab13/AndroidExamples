package proveidor_dades.Proveidor;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;

/**
 * Created by abde on 5/04/16.
 */
public class ProveidorsContentProvider  extends ContentProvider {
    PrincipalSQLiteHelper openHelper;
    @Override
    public boolean onCreate() {
        openHelper=new PrincipalSQLiteHelper(getContext());
        return openHelper!=null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(Constants.TAULA_PROVEEDORES);

        int tipusUri = ProveidorsContract.sUriMatcher.match(uri);

        switch(tipusUri)
        {
            case ProveidorsContract.TOTES_LES_FILES:
                break;
            case ProveidorsContract.UNA_FILA:
                queryBuilder.appendWhere(Constants.PROVEEDORES_ID_PROVEEDOR+ " = " +uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        SQLiteDatabase bd = openHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(bd, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String resultat;
        int tipusUri=ProveidorsContract.sUriMatcher.match(uri);

        switch (tipusUri)
        {
            case ProveidorsContract.TOTES_LES_FILES:
                resultat=ProveidorsContract.MIME_MULTIPLE;
                break;
            case ProveidorsContract.UNA_FILA:
                resultat=ProveidorsContract.MIME_UNA;
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }
        return resultat;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int tipusUri = ProveidorsContract.sUriMatcher.match(uri);
        long id = 0;

        switch(tipusUri)
        {
            case ProveidorsContract.TOTES_LES_FILES:
                SQLiteDatabase db = openHelper.getWritableDatabase();
                id = db.insert(Constants.TAULA_PROVEEDORES, null, values);
                break;
            case ProveidorsContract.UNA_FILA:
                throw new IllegalArgumentException("Uri incorrecte: " + uri);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(ProveidorsContract.CONTENT_URI, id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int tipusUri = ProveidorsContract.sUriMatcher.match(uri);
        long filesEsborrades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case ProveidorsContract.TOTES_LES_FILES:
                filesEsborrades = db.delete(Constants.TAULA_PROVEEDORES, selection, selectionArgs);
                break;
            case ProveidorsContract.UNA_FILA:
                filesEsborrades = db.delete(Constants.TAULA_PROVEEDORES, Constants.PROVEEDORES_ID_PROVEEDOR + " = " +uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int tipusUri = ProveidorsContract.sUriMatcher.match(uri);
        int filesActualitzades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case ProveidorsContract.TOTES_LES_FILES:
                filesActualitzades = db.update(Constants.TAULA_PROVEEDORES, values, selection, selectionArgs);
                break;
            case ProveidorsContract.UNA_FILA:
                filesActualitzades = db.update(Constants.TAULA_PROVEEDORES, values, Constants.PROVEEDORES_ID_PROVEEDOR+ " = " + uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return filesActualitzades;
    }
}
