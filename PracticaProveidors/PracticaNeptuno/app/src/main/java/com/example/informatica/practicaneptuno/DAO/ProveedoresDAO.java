package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Proveedores;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class ProveedoresDAO
{
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String [] columnes =
            {
                    Constants.COLUMNA_ID,
                    Constants.PROVEEDORES_ID_PROVEEDOR,
                    Constants.PROVEEDORES_NOMBRE_COMPANIA,
                    Constants.PROVEEDORES_NOMBRE_CONTACTO,
                    Constants.PROVEEDORES_CARGO_CONTACTO,
                    Constants.PROVEEDORES_DIRECCION,
                    Constants.PROVEEDORES_CIUDAD,
                    Constants.PROVEEDORES_REGION,
                    Constants.PROVEEDORES_COD_POSTAL,
                    Constants.PROVEEDORES_PAIS,
                    Constants.PROVEEDORES_TELEFONO,
                    Constants.PROVEEDORES_FAX,
                    Constants.PROVEEDORES_PAGINA_PRINCIPAL
            };

    public ProveedoresDAO(Context context)
    {
        bdHelper = new PrincipalSQLiteHelper(context);
    }

    public void obre()
    {
        bd = bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Proveedores cursorAProveedores(Cursor cursor)
    {
        return new Proveedores(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10),
                cursor.getString(11), cursor.getString(12));
    }

    public Proveedores creaProveedores (int idProveedor, String nombreCompania, String nombreContacto, String cargoContacto, String direccion, String ciudad, String region, String codPostal, String pais, String telefono, String fax, String paginaPrincipal)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.PROVEEDORES_ID_PROVEEDOR, idProveedor);
        valors.put(Constants.PROVEEDORES_NOMBRE_COMPANIA, nombreCompania);
        valors.put(Constants.PROVEEDORES_NOMBRE_CONTACTO, nombreContacto);
        valors.put(Constants.PROVEEDORES_CARGO_CONTACTO, cargoContacto);
        valors.put(Constants.PROVEEDORES_DIRECCION, direccion);
        valors.put(Constants.PROVEEDORES_CIUDAD, ciudad);
        valors.put(Constants.PROVEEDORES_REGION, region);
        valors.put(Constants.PROVEEDORES_COD_POSTAL, codPostal);
        valors.put(Constants.PROVEEDORES_PAIS, pais);
        valors.put(Constants.PROVEEDORES_TELEFONO, telefono);
        valors.put(Constants.PROVEEDORES_FAX, fax);
        valors.put(Constants.PROVEEDORES_PAGINA_PRINCIPAL, paginaPrincipal);


        long idInsercio = bd.insert(
                Constants.TAULA_PROVEEDORES,
                null,
                valors
        );

        if (idInsercio == -1)
        {
            return null;
        }

        else
        {
            Cursor cursor = bd.query(
                    Constants.TAULA_PROVEEDORES,
                    columnes, Constants.PROVEEDORES_ID_PROVEEDOR + " = '" + idProveedor + "'",
                    null, null, null, null
            );

            cursor.moveToFirst();
            Proveedores proveedores = cursorAProveedores(cursor);
            cursor.close();

            return proveedores;
        }
    }

    public Proveedores updateProveedores (int idProveedorAnteror, int idProveedor, String nombreCompania, String nombreContacto, String cargoContacto, String direccion, String ciudad, String region, String codPostal, String pais, String telefono, String fax, String paginaPrincipal)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.PROVEEDORES_ID_PROVEEDOR, idProveedor);
        valors.put(Constants.PROVEEDORES_NOMBRE_COMPANIA, nombreCompania);
        valors.put(Constants.PROVEEDORES_NOMBRE_CONTACTO, nombreContacto);
        valors.put(Constants.PROVEEDORES_CARGO_CONTACTO, cargoContacto);
        valors.put(Constants.PROVEEDORES_DIRECCION, direccion);
        valors.put(Constants.PROVEEDORES_CIUDAD, ciudad);
        valors.put(Constants.PROVEEDORES_REGION, region);
        valors.put(Constants.PROVEEDORES_COD_POSTAL, codPostal);
        valors.put(Constants.PROVEEDORES_PAIS, pais);
        valors.put(Constants.PROVEEDORES_TELEFONO, telefono);
        valors.put(Constants.PROVEEDORES_FAX, fax);
        valors.put(Constants.PROVEEDORES_PAGINA_PRINCIPAL, paginaPrincipal);


        long idInsercio = bd.update(
                Constants.TAULA_PROVEEDORES,
                valors, Constants.PROVEEDORES_ID_PROVEEDOR + " = " + idProveedorAnteror, null
        );

        if (idInsercio == -1)
        {
            return null;
        }

        else
        {
            Cursor cursor = bd.query(
                    Constants.TAULA_PROVEEDORES,
                    columnes, Constants.PROVEEDORES_ID_PROVEEDOR + " = '" + idProveedor + "'",
                    null, null, null, null
            );

            cursor.moveToFirst();
            Proveedores proveedores = cursorAProveedores(cursor);
            cursor.close();

            return proveedores;
        }
    }

    public boolean eliminaProveedores (Proveedores proveedores)
    {
        /*int nEsborrats = bd.delete(Constants.TAULA_PROVEEDORES,
                Constants.PROVEEDORES_ID_PROVEEDOR + " = '" + proveedores.getIdProveedor() + "'",
                null);*/
        return eliminaProveedores(proveedores.getIdProveedor());
        //return nEsborrats > 0;
    }
    public boolean eliminaProveedores (int id)
    {
        int nEsborrats = bd.delete(Constants.TAULA_PROVEEDORES,
                Constants.PROVEEDORES_ID_PROVEEDOR + " = '" + id + "'",
                null);

        return nEsborrats > 0;
    }

    public List<Proveedores> obteProveedores()
    {
        List<Proveedores> proveedores = new ArrayList<Proveedores>();

        Cursor cursor = bd.query(Constants.TAULA_PROVEEDORES,
                columnes,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Proveedores proveedor = cursorAProveedores(cursor);
            proveedores.add(proveedor);
            cursor.moveToNext();
        }

        return proveedores;
    }
    public int EnDepenen(int id)
    {
        Cursor mCursor=bd.rawQuery("SELECT COUNT(*) FROM "+Constants.TAULA_PRODUCTOS+" WHERE "+Constants.PRODUCTOS_ID_PROVEEDOR+" = "+id, null);
        mCursor.moveToFirst();
        int depenen= mCursor.getInt(0);
        mCursor.close();
        return depenen;
    }
}