package com.example.informatica.practicaneptuno.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorCategories;
import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorProductes;
import com.example.informatica.practicaneptuno.Categories;
import com.example.informatica.practicaneptuno.DAO.CategoriasDAO;
import com.example.informatica.practicaneptuno.DAO.ProductosDAO;
import com.example.informatica.practicaneptuno.POJO.Categorias;
import com.example.informatica.practicaneptuno.ProductesPerCategoria;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 28/03/16.
 */
public class CategoriesDetall extends Fragment {
    ListView lvCategories, lvProductes;
    CategoriasDAO categoriasDAO;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.categories_detall, container, false);
        categoriasDAO=new CategoriasDAO(getActivity());
        categoriasDAO.obre();
        lvCategories= (ListView) view.findViewById(R.id.lvCategories);
        lvProductes= (ListView) view.findViewById(R.id.lvProductes);
        lvCategories.setAdapter(new AdaptadorCategories(categoriasDAO.obteCategorias(), getActivity()));
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductosDAO productosDAO=new ProductosDAO(getActivity());
                productosDAO.obre();
                lvProductes.setAdapter(new AdaptadorProductes(productosDAO.obteProductosAmbCategoria((int)lvCategories.getItemIdAtPosition(position)), getActivity()));
            }
        });
        return view;
    }
}
