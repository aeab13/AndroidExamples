package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Empleados implements Parcelable
{
    private int idEmpleado;
    private String apellidos;
    private String nombre;
    private String cargo;
    private String tratamiento;
    private String fechaNacimiento;
    private String fechaContratacion;
    private String direccion;
    private String ciudad;
    private String region;
    private String codPostal;
    private String pais;
    private String telDomicilio;
    private String extension;
    private String foto;
    private String notas;
    private int jefe;

    public Empleados() {
    }

    public Empleados(int idEmpleado, String apellidos, String nombre, String cargo, String tratamiento, String fechaNacimiento, String fechaContratacion, String direccion, String ciudad, String region, String codPostal, String pais, String telDomicilio, String extension, String foto, String notas, int jefe) {
        this.idEmpleado = idEmpleado;
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.cargo = cargo;
        this.tratamiento = tratamiento;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaContratacion = fechaContratacion;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.region = region;
        this.codPostal = codPostal;
        this.pais = pais;
        this.telDomicilio = telDomicilio;
        this.extension = extension;
        this.foto = foto;
        this.notas = notas;
        this.jefe = jefe;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(String fechaCotnratacion) {
        this.fechaContratacion = fechaCotnratacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelDomicilio() {
        return telDomicilio;
    }

    public void setTelDomicilio(String telDomicilio) {
        this.telDomicilio = telDomicilio;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getJefe() {
        return jefe;
    }

    public void setJefe(int jefe) {
        this.jefe = jefe;
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idEmpleado);
        dest.writeString(apellidos);
        dest.writeString(nombre);
        dest.writeString(cargo);
        dest.writeString(tratamiento);
        dest.writeString(fechaNacimiento);
        dest.writeString(fechaContratacion);
        dest.writeString(direccion);
        dest.writeString(ciudad);
        dest.writeString(region);
        dest.writeString(codPostal);
        dest.writeString(pais);
        dest.writeString(telDomicilio);
        dest.writeString(extension);
        dest.writeString(foto);
        dest.writeString(notas);
        dest.writeInt(jefe);
    }

    public void readFromParcel (Parcel dades)
    {
        idEmpleado = dades.readInt();
        apellidos = dades.readString();
        nombre = dades.readString();
        cargo = dades.readString();
        tratamiento = dades.readString();
        fechaNacimiento = dades.readString();
        fechaContratacion = dades.readString();
        direccion = dades.readString();
        ciudad = dades.readString();
        region = dades.readString();
        codPostal = dades.readString();
        pais = dades.readString();
        telDomicilio = dades.readString();
        extension = dades.readString();
        foto = dades.readString();
        notas = dades.readString();
        jefe = dades.readInt();
    }
    public Empleados(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<Empleados> CREATOR = new Creator<Empleados>() {
        @Override
        public Empleados createFromParcel(Parcel source) {
            return new Empleados(source);
        }

        @Override
        public Empleados[] newArray(int size) {
            return new Empleados[size];
        }
    };

}
