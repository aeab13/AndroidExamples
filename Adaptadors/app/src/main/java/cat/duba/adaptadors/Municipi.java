package cat.duba.adaptadors;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abde on 15/12/15.
 */
public class Municipi implements Parcelable{
    private int codiProvincia;
    private int codiINE;
    private String nomProvincia;
    private String nomMunicipi;

    public Municipi()
    {}

    public Municipi(int codiProvincia, int codiINE, String nomProvincia, String nomMunicipi)
    {
        this.codiProvincia=codiProvincia;
        this.codiINE=codiINE;
        this.nomProvincia=nomProvincia;
        this.nomMunicipi=nomMunicipi;
    }

    protected Municipi(Parcel in) {
        codiProvincia = in.readInt();
        codiINE = in.readInt();
        nomProvincia = in.readString();
        nomMunicipi = in.readString();
    }

    public static final Creator<Municipi> CREATOR = new Creator<Municipi>() {
        @Override
        public Municipi createFromParcel(Parcel in) {
            return new Municipi(in);
        }

        @Override
        public Municipi[] newArray(int size) {
            return new Municipi[size];
        }
    };

    @Override
    public String toString(){
        return nomMunicipi;
    }

    public int getCodiProvincia() {
        return codiProvincia;
    }

    public void setCodiProvincia(int codiProvincia) {
        this.codiProvincia = codiProvincia;
    }

    public int getCodiINE() {
        return codiINE;
    }

    public void setCodiINE(int codiINE) {
        this.codiINE = codiINE;
    }

    public String getNomProvincia() {
        return nomProvincia;
    }

    public void setNomProvincia(String nomProvincia) {
        this.nomProvincia = nomProvincia;
    }

    public String getNomMunicipi() {
        return nomMunicipi;
    }

    public void setNomMunicipi(String nomMunicipi) {
        this.nomMunicipi = nomMunicipi;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(codiProvincia);
        dest.writeInt(codiINE);
        dest.writeString(nomProvincia);
        dest.writeString(nomMunicipi);
    }

}
