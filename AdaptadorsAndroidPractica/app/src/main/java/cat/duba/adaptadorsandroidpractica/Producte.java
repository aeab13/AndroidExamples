package cat.duba.adaptadorsandroidpractica;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abde on 18/12/15.
 */

public class Producte implements Parcelable {

    private int codiProducte;
    private String nomProducte;
    private String categoria;
    private double preu;
    private int estoc;

    public Producte() {

    }

    public Producte(int codiProducte, String nomProducte, String categoria, double preu, int estoc)
    {
        this.codiProducte = codiProducte;
        this.nomProducte = nomProducte;
        this.categoria = categoria;
        this.preu = preu;
        this.estoc = estoc;
    }

    protected Producte(Parcel in)
    {
        codiProducte = in.readInt();
        nomProducte = in.readString();
        categoria = in.readString();
        preu = in.readDouble();
        estoc = in.readInt();
    }

    public static final Parcelable.Creator<Producte> CREATOR = new Creator<Producte>()
    {
        @Override
        public Producte createFromParcel(Parcel in) { return new Producte(in); }

        @Override
        public Producte[] newArray(int size) { return new Producte[size]; }
    };

    @Override
    public String toString() { return nomProducte; }

    public int getCodiProducte() { return codiProducte; }

    public void setCodiProducte(int codiProducte) { this.codiProducte = codiProducte; }

    public String getNomProducte() { return nomProducte; }

    public void setNomProducte(String nomProducte) { this.nomProducte = nomProducte; }

    public String getCategoria() { return categoria; }

    public void setCategoria() { this.categoria = categoria; }

    public double getPreu() { return preu; }

    public void setPreu(double preu) { this.preu = preu; }

    public int getEstoc() { return estoc; }

    public void setEstoc(int estoc) { this.estoc = estoc; }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(codiProducte);
        dest.writeString(nomProducte);
        dest.writeString(categoria);
        dest.writeDouble(preu);
        dest.writeInt(estoc);
    }
}