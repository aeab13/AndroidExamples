package cat.duba.fuckinggoogleservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private final String CLIENT_ID="649870716660-gu0173q6osebhaeome7cuikjpeib5g9a.apps.googleusercontent.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.mnuGPlus:
                startActivity(new Intent(this, GPlusActivity.class));
                return  true;
            case R.id.mnuValidacio:
                startActivity(new Intent(this, ValidacioActivity.class));
                return true;
        }
        return false;
    }
}
