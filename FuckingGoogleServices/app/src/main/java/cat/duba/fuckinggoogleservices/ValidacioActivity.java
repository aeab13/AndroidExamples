package cat.duba.fuckinggoogleservices;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ValidacioActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private AccountManager accountManager;
    private TextView tvCompte;
    private ListView lvLlista;
    private Account compte;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validacio);

        tvCompte= (TextView) findViewById(R.id.tvCompte);
        lvLlista= (ListView) findViewById(R.id.lvLlista);

        lvLlista.setOnItemSelectedListener(this);

        obteComptes();

    }

    private void obteComptes() {
        accountManager= AccountManager.get(this);
        Account[] comptes = accountManager.getAccountsByType("com.google");

        ArrayAdapter<Account> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, comptes);

        lvLlista.setAdapter(adaptador);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        compte= (Account) parent.getItemAtPosition(position);
        accountManager.getAuthToken(compte,
                "Manage your tasks", new Bundle(), this, new OnTokenAdquited(), new Handler(new OnError()));
    }

    /*
    * This method is useless and it should be deleted because it sux
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class OnError implements Handler.Callback {
        @Override
        public boolean handleMessage(Message msg) {
            return false;
        }
    }

    private class OnTokenAdquited implements AccountManagerCallback<Bundle> {
        @Override
        public void run(AccountManagerFuture<Bundle> future) {

        }
    }
}
