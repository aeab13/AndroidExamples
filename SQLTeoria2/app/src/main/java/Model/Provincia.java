package Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sergi on 14/03/2016.
 */

public class Provincia implements Parcelable{
    private int codiProvincia;
    private String nomProvincia;

    public Provincia(){}
    public Provincia(int codi,String nom) {
        this.codiProvincia = codi;
        this.nomProvincia = nom;
    }

    protected Provincia(Parcel in) {
        codiProvincia = in.readInt();
        nomProvincia = in.readString();
    }
    public int getCodiProvincia() {
        return codiProvincia;
    }

    public void setCodiProvincia(int codiProvincia) {
        this.codiProvincia = codiProvincia;
    }

    public String getNomProvincia() {
        return nomProvincia;
    }

    public void setNomProvincia(String nomProvincia) {
        this.nomProvincia = nomProvincia;
    }
    @Override
    public String toString(){
        return nomProvincia;
    }
    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(codiProvincia);
        dest.writeString(nomProvincia);
    }
    public void readFromParcel(Parcel dades){
        codiProvincia =dades.readInt();
        nomProvincia =dades.readString();
    }
    public static final Creator<Provincia> CREATOR = new Creator<Provincia>() {
        @Override
        public Provincia createFromParcel(Parcel in) {
            return new Provincia(in);
        }

        @Override
        public Provincia[] newArray(int size) {
            return new Provincia[size];
        }
    };
}
