package proveidor_dades.Municipi;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by SergiDAM on 29/03/2016.
 */
public class MunicipiContract {
    private MunicipiContract()
    {}

    public static final String ESQUEMA = "content://";

    public static final String AUTHORITY = "proveidor_dades.Municipi";

    public static final String BASE_PATH = "Municipi";

    public static final Uri CONTENT_URI = Uri.parse(ESQUEMA+AUTHORITY+"/"+BASE_PATH);

    public static final int TOTES_LES_FILES = 1;

    public static final int UNA_FILA = 2;

    public static final UriMatcher sUriMatcher;
    static
    {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, TOTES_LES_FILES);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH+"/#", UNA_FILA);
    }

    public final static class Columnes implements BaseColumns
    {
        public static final String COLUMNA_ID_PROVINCIA = "id_provincia";
        public static final String COLUMNA_ID_MUNICIPI = "id_municipi";

        public static final String COLUMNA_NOM_MUNICIPI = "municipi";
    }

    public final static String MIME_UNA = "vnd.android.cursos.item/vnd." +AUTHORITY+"/"+BASE_PATH;

    public final static String MIME_MULTIPLE = "vnd.android.cursos.dir/vnd." +AUTHORITY+"/"+BASE_PATH;
}
