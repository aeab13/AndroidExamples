package proveidor_dades.Provincia;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteTransactionListener;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.example.sergi.sqlteoria.AppConstants;
import com.example.sergi.sqlteoria.MunicipisSQLHelper;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by SergiDAM on 29/03/2016.
 */
public class ProvinciaContentProvider extends ContentProvider {
    MunicipisSQLHelper openHelper;
    @Override
    public boolean onCreate() {
        openHelper = new MunicipisSQLHelper(getContext());
        return openHelper != null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(AppConstants.TAULA_PROVINCIES);

        int tipusUri = ProvinciaContract.sUriMatcher.match(uri);

        switch(tipusUri)
        {
            case ProvinciaContract.TOTES_LES_FILES:
                break;
            case ProvinciaContract.UNA_FILA:
                queryBuilder.appendWhere(AppConstants.COLUMNA_ID_PROVINCIA + " = " +uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        SQLiteDatabase bd = openHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(bd, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String resultat;
        int tipusUri = ProvinciaContract.sUriMatcher.match(uri);

        switch(tipusUri)
        {
            case ProvinciaContract.TOTES_LES_FILES:
                resultat = ProvinciaContract.MIME_MULTIPLE;
                break;
            case ProvinciaContract.UNA_FILA:
                resultat = ProvinciaContract.MIME_UNA;
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        return resultat;
    }
    private void comprovaColumnes (String[] projection)//Comprovacio nom de columnes
    {
        String[] disponibles = {AppConstants.COLUMNA_ID, AppConstants.COLUMNA_ID_PROVINCIA, AppConstants.COLUMNA_NOM_PROVINCIA};

        if(projection != null)
        {
            HashSet<String> columnesDemanades = new HashSet<>(Arrays.asList(projection));
            HashSet<String> columnesDisponibles = new HashSet<>(Arrays.asList(disponibles));

            if(!columnesDisponibles.containsAll(columnesDemanades))
            {
                throw new IllegalArgumentException("Hi ha columnes desconegudes a la projeccio.");
            }
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int tipusUri = ProvinciaContract.sUriMatcher.match(uri);
        long id = 0;

        switch(tipusUri)
        {
            case ProvinciaContract.TOTES_LES_FILES:
                SQLiteDatabase db = openHelper.getWritableDatabase();
                id = db.insert(AppConstants.TAULA_PROVINCIES, null, values);
                break;
            case ProvinciaContract.UNA_FILA:
                throw new IllegalArgumentException("Uri incorrecte: " + uri);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(ProvinciaContract.CONTENT_URI, id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int tipusUri = ProvinciaContract.sUriMatcher.match(uri);
        long filesEsborrades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case ProvinciaContract.TOTES_LES_FILES:
                filesEsborrades = db.delete(AppConstants.TAULA_PROVINCIES, selection, selectionArgs);
                break;
            case ProvinciaContract.UNA_FILA:
                filesEsborrades = db.delete(AppConstants.TAULA_PROVINCIES, AppConstants.COLUMNA_ID_PROVINCIA + " = " +uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int tipusUri = ProvinciaContract.sUriMatcher.match(uri);
        int filesActualitzades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case ProvinciaContract.TOTES_LES_FILES:
                filesActualitzades = db.update(AppConstants.TAULA_PROVINCIES, values, selection, selectionArgs);
                break;
            case ProvinciaContract.UNA_FILA:
                filesActualitzades = db.update(AppConstants.TAULA_PROVINCIES, values, AppConstants.COLUMNA_ID_PROVINCIA + " = " + uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return filesActualitzades;
    }
}
