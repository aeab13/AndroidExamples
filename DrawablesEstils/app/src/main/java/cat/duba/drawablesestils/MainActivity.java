package cat.duba.drawablesestils;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clicDeImatge(View vista)
    {
        ImageView iv=(ImageView) vista;
        switch (vista.getId())
        {
            case R.id.ivNivells:
                int nivell=iv.getDrawable().getLevel();
                iv.getDrawable().setLevel((nivell+1)%4);
                break;
        }
    }
}
