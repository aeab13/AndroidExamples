package DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sergi.sqlteoria.AppConstants;
import com.example.sergi.sqlteoria.MunicipisSQLHelper;

import java.util.ArrayList;
import java.util.List;

import Model.Provincia;

/**
 * Created by Sergi on 15/03/2016.
 */
public class ProvinciaDAO
{
    private SQLiteDatabase db;
    private SQLiteOpenHelper dbHelper;
    private String[] columnes = {AppConstants.COLUMNA_ID, AppConstants.COLUMNA_ID_PROVINCIA, AppConstants.COLUMNA_NOM_PROVINCIA};

    public ProvinciaDAO(Context context)
    {
        dbHelper = new MunicipisSQLHelper(context);
    }

    public void obre()
    {
        db = dbHelper.getWritableDatabase();
    }

    public void tanca()
    {
        db.close();
    }

    public Provincia cursorAProvincia(Cursor cursor)
    {
        Provincia provincia = new Provincia();
        provincia.setCodiProvincia(cursor.getInt(1));
        provincia.setNomProvincia(cursor.getString(2));

        return provincia;
    }

    public Provincia creaProvincia (int codiProvincia, String nomProvincia)
    {
        ContentValues valors = new ContentValues();
        valors.put(AppConstants.COLUMNA_ID_PROVINCIA, codiProvincia);
        valors.put(AppConstants.COLUMNA_NOM_PROVINCIA, nomProvincia);

        long idInsercio = db.insert(AppConstants.TAULA_PROVINCIES, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = db.query(AppConstants.TAULA_PROVINCIES, columnes, AppConstants.COLUMNA_ID_PROVINCIA + " = '" + codiProvincia + "'",
                    null, null, null, null);

            cursor.moveToFirst();

            Provincia provincia = cursorAProvincia(cursor);
            cursor.close();
            return provincia;
        }
    }

    public boolean eliminaProvincia (Provincia provincia)
    {
        int nEsborrats = db.delete(AppConstants.TAULA_PROVINCIES, AppConstants.COLUMNA_ID_PROVINCIA + " = '" + provincia.getCodiProvincia() +"'", null);
        return nEsborrats >0;
    }

    public List<Provincia> obteProvincies()
    {
        List<Provincia> provincies = new ArrayList<>();

        Cursor cursor = db.query(AppConstants.TAULA_PROVINCIES, columnes, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            Provincia provincia = cursorAProvincia(cursor);
            provincies.add(provincia);
            cursor.moveToNext();
        }

        return provincies;
    }
}


