package com.example.sergi.sqlteoria;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;

import Model.Municipi;

/**
 * Created by Xevi on 15/03/2015.
 */
public class Fitxers {
    public static final String MUNICIPI = "Municipis";
    public static final String LIST_MUNICIPIS = "LlistaDeMunicipis";
    public static final String MAP_MUNICIPIS = "DiccionariDeMunicipis";
    public static Random rnd = new Random();


    public static ArrayList<Municipi> obtenirMunicipis(Context contexte) {
        ArrayList<Municipi> municipis = new ArrayList<Municipi>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  contexte.getResources().openRawResource(R.raw.municipis);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                municipis.add(new Municipi(Integer.parseInt(camps[0]), Integer.parseInt(camps[3]), camps[1], camps[2]));
            }
            fitxerRaw.close();
        } catch (Exception ex) {
        }

        return municipis;
    }

    public static Map<String,ArrayList<Municipi>> obtenirMunicipisClassificats(Context contexte)
    {
        Map<String,ArrayList<Municipi>> municipisExp= new Hashtable<String, ArrayList<Municipi>>();
        ArrayList<Municipi> municipis = Fitxers.obtenirMunicipis(contexte);
        for(Municipi m:municipis)
        {
            if (!municipisExp.containsKey(m.getNomProvincia())) {
                municipisExp.put(m.getNomProvincia(), new ArrayList<Municipi>());
            }

            ArrayList<Municipi> taula =  municipisExp.get(m.getNomProvincia());
            taula.add(m);
        }
        return municipisExp;
    }

    private static Integer codiProvincia (int codiINE)
    {
        Integer resultat = codiINE/1000;
        return resultat;
    }
    public static Map<Integer, String> obtenirTaulaProvincies (Context contexte)
    {
        Map<Integer, String> provincies = new Hashtable<Integer, String>();
        ArrayList<Municipi> municipis = Fitxers.obtenirMunicipis(contexte);
        for(Municipi m:municipis)
        {
            if (!provincies.containsKey(codiProvincia(m.getCodiIne()))) {
                provincies.put(codiProvincia(m.getCodiIne()), m.getNomProvincia());
            }
        }
        return provincies;
    }

    public static int obtenirDrawableAleatori()
    {
        int[] dibuixets = new int[] {
                android.R.drawable.alert_light_frame,
                android.R.drawable.btn_default_small,
                android.R.drawable.btn_star ,
                android.R.drawable.dialog_frame,
                android.R.drawable.ic_delete,
                android.R.drawable.ic_dialog_alert,
                android.R.drawable.ic_menu_day,
                android.R.drawable.radiobutton_on_background,
                android.R.drawable.ic_notification_overlay,
                android.R.drawable.radiobutton_off_background
        };

        return dibuixets[rnd.nextInt(10)];
    }
}