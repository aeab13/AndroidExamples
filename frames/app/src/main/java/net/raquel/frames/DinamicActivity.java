package net.raquel.frames;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import net.raquel.frames.cosetes.Municipi;

public class DinamicActivity extends AppCompatActivity implements LlistaDeMunicipisFragment.OnMunicipiListener {
    FrameLayout contenidorLlistaMunicipis, contenidorMunicipi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dinamic);

        contenidorLlistaMunicipis= (FrameLayout) findViewById(R.id.contenidorLlista);
        contenidorMunicipi= (FrameLayout) findViewById(R.id.contenidorMunicpi);
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            contenidorMunicipi.setVisibility(View.VISIBLE);
        }
        else
        {
            contenidorMunicipi.setVisibility(View.GONE);
        }
        if(savedInstanceState==null){
        FragmentTransaction manegadorFragents=getFragmentManager().beginTransaction();
        manegadorFragents.add(R.id.contenidorLlista, new LlistaDeMunicipisFragment());
        manegadorFragents.commit();}
    }

    @Override
    public void OnMunicipiTocat(Municipi municipi) {
/*        Fragment fragment=MunipiciFragment.newInstance(municipi);
        if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

        }*/
        //Si esta horitz, posem el fragment del municipi tocat al costat, creem el fragment del municipi tocat


        Fragment frgMunicipi = MunipiciFragment.newInstance(municipi);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            //Obtenim una transaccio
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.contenidorMunicpi, frgMunicipi);
            ft.commit();
        }
        else{
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.contenidorLlista, frgMunicipi);
            ft.commit();
        }

    }
}
