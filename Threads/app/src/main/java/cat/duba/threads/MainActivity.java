package cat.duba.threads;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.mnuThreads)
        {
            startActivity(new Intent(this, ThreadsActivity.class));
        }
        else if(item.getItemId()==R.id.mnuLoaders)
        {
            startActivity(new Intent(this, LoadersActivity.class));
        }
        return  super.onOptionsItemSelected(item);
    }
}
