package cat.duba.threads;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ThreadsActivity extends AppCompatActivity {

    ProgressBar pbProgressBar;
    TextView tvText;
    Handler manegadorThreadIU;
    Button btnCancelaAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threads);
        pbProgressBar= (ProgressBar) findViewById(R.id.pbProgressBar);
        tvText= (TextView) findViewById(R.id.tvText);
        btnCancelaAsyncTask= (Button) findViewById(R.id.btnCancelaAsyncTask);

        manegadorThreadIU=new Handler();
    }

    public void btnIniciAmbHandlers_Click(View vista)
    {

        Runnable runnable=new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i <= 100; i=i+10) {
                    final int valor=i;
                    procesCostos();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pbProgressBar.setProgress(valor);
                            tvText.setText("S'ha carregat un " + valor + "% de les dades.");
                        }
                    });
                }

                manegadorThreadIU.post(new Runnable() {
                    @Override
                    public void run() {
                        tvText.setText("Ha finalitzat la càrrega");
                    }
                });

            }
        };
        tvText.setText("Iniciant càrrega de dades");
        pbProgressBar.setProgress(0);
        new Thread(runnable).start();

    }

    private void procesCostos()
    {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void btnIniciaTascaAssincrona_Click(View vista)
    {
        TascaAssincrona ta=new TascaAssincrona();
        //ta.execute();
        ta.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class TascaAssincrona extends AsyncTask<Void, Integer, Boolean>
    {

        @Override
        protected Boolean doInBackground(Void... params) {
            int i=0;
            while ((i<=100)&&!isCancelled())
            {
                procesCostos();
                i+=10;
                publishProgress(i);
            }
            return i>100;

        }

        @Override
        protected void onPreExecute() {
            tvText.setText("Iniciant càrrega de dades.");
            pbProgressBar.setProgress(0);
            btnCancelaAsyncTask.setEnabled(true);
            btnCancelaAsyncTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TascaAssincrona.this.cancel(true);
                }
            });
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int valor=values[0].intValue();
            pbProgressBar.setProgress(valor);
            tvText.setText("S'ha carregat un " + valor + "% de les dades.");
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            tvText.setText("Ha finalitzat la càrrega de dades.");
            btnCancelaAsyncTask.setEnabled(false);
            btnCancelaAsyncTask.setOnClickListener(null);
        }
    }
}
