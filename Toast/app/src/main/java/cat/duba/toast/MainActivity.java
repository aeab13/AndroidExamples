package cat.duba.toast;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnPrimer, btnSegon, btnTercer, btnQuart, btnCinque;
    private final int DADES=1,DADES2=2;
    final String NOM="Nom";
    private String nom, cognom, sexe, telefon, email, adreça;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPrimer= (Button) findViewById(R.id.btnPrimer);
        btnSegon= (Button) findViewById(R.id.btnSegon);
        btnTercer= (Button) findViewById(R.id.btnTercer);
        btnQuart= (Button) findViewById(R.id.btnQuart);
        btnCinque= (Button) findViewById(R.id.btnCinque);
        btnPrimer.setOnClickListener(this);
        btnSegon.setOnClickListener(this);
        btnTercer.setOnClickListener(this);
        btnQuart.setOnClickListener(this);
        btnCinque.setOnClickListener(this);

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode)
        {
            case DADES:
                if(resultCode==RESULT_OK)
                {
                    nom=data.getStringExtra(NOM);
                    cognom=data.getStringExtra("COGNOM");
                    sexe=data.getStringExtra("SEXE");
                }
                break;
            case DADES2:
                if(resultCode==RESULT_OK) {
                    adreça = data.getStringExtra("ADREÇA");
                    telefon = data.getStringExtra("TELEFON");
                    email = data.getStringExtra("EMAIL");
                }
                break;
        }
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.btnPrimer:
                intent=new Intent(this, PrimerActivity.class);
                startActivityForResult(intent, DADES);
                break;
            case R.id.btnSegon:
                intent=new Intent(this, SegonActivity.class);
                startActivityForResult(intent, DADES2);
                break;
            case R.id.btnTercer:
                if(nom!=null&&cognom!=null&&sexe!=null&&adreça!=null&&telefon!=null&&email!=null)
                {
                    LayoutInflater inflater = getLayoutInflater();

                    View layout = inflater.inflate(R.layout.torrada,
                            (ViewGroup) findViewById(R.id.llCustom));

                    TextView text = (TextView) layout.findViewById(R.id.tvNom);
                    text.setText(nom);
                    text = (TextView) layout.findViewById(R.id.tvCognom);
                    text.setText(cognom);
                    text = (TextView) layout.findViewById(R.id.tvSexe);
                    text.setText(sexe);
                    text = (TextView) layout.findViewById(R.id.tvAdreça);
                    text.setText(adreça);
                    text = (TextView) layout.findViewById(R.id.tvEmail);
                    text.setText(email);
                    text = (TextView) layout.findViewById(R.id.tvTelefon);
                    text.setText(telefon);

                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                }
                else
                    Toast.makeText(this, "No s'han entrat totes les dades", Toast.LENGTH_LONG).show();
                break;
            case R.id.btnQuart:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.es/")));
                break;
            case R.id.btnCinque:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:999999999")));
                break;
        }

    }
}
