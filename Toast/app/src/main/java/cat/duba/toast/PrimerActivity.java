package cat.duba.toast;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class PrimerActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etNom, etCognom;
    RadioButton rbtHome, rbtDona;
    Button btnAcceptar, btnCancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primer);
        etNom= (EditText) findViewById(R.id.etNom);
        etCognom= (EditText) findViewById(R.id.etCognom);
        rbtDona= (RadioButton) findViewById(R.id.rbtDona);
        rbtHome= (RadioButton) findViewById(R.id.rbtHome);
        btnAcceptar= (Button) findViewById(R.id.btnAccept);
        btnCancelar= (Button) findViewById(R.id.btnCancel);
        btnAcceptar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
    }
    public void onClick(View v) {
        Intent intent=getIntent();
        switch (v.getId())
        {
            case R.id.btnAccept:
                intent.putExtra("NOM", etNom.getText().toString());
                intent.putExtra("COGNOM", etCognom.getText().toString());
                if(rbtDona.isChecked()) intent.putExtra("SEXE", "Dona");
                else if(rbtHome.isChecked()) intent.putExtra("SEXE", "Home");
                break;
        }
        setResult(RESULT_OK, intent);
        finish();
    }
}
