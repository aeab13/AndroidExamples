package cat.duba.adaptadorsandroidpractica;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class ExpandableAdapter extends AppCompatActivity {

    ExpandableListView elvProductes;
    ArrayList<Producte> productesArray;
    Map<String,ArrayList<Producte>> categories =new Hashtable<String, ArrayList<Producte>>();
    Intent pasarProducte;
    final static String PRODUCTE="Producte";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Municipi producte
        //provincia categoria
        setContentView(R.layout.activity_expandable_adapter);
        pasarProducte=new Intent(this, dades.class);
        Intent intent=getIntent();
        productesArray = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);
        classificaMunicipis();
        elvProductes = (ExpandableListView) findViewById(R.id.elvProductes);
        elvProductes.setAdapter(new AdaptadorExpandibleMunicipis(this, categories));
    }

    private void classificaMunicipis() {
        for(Producte p: productesArray)
        {
            if(!categories.containsKey(p.getCategoria()))
            {
                categories.put(p.getCategoria(), new ArrayList<Producte>());
            }

            ArrayList<Producte> taula= categories.get(p.getCategoria());
            taula.add(p);
        }
    }

    private class AdaptadorExpandibleMunicipis extends BaseExpandableListAdapter
    {

        Map<String, ArrayList<Producte>> categories;
        Context context;
        String[] claus=new String[0];
        private AdaptadorExpandibleMunicipis(
                Context context, Map<String, ArrayList<Producte>> provincies)
        {
            this.context=context;
            this.categories =provincies;
            claus=provincies.keySet().toArray(claus);
        }

        @Override
        public int getGroupCount() {
            return categories.keySet().size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            String nomProvincia=claus[groupPosition];
            ArrayList<Producte> municipisDeProv= categories.get(nomProvincia);

            return municipisDeProv.size();
            //return categories.get(claus[groupPosition]).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return categories.get(claus[groupPosition]);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return categories.get(claus[groupPosition]).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            ContenidorProvincia contenidor;
            if(convertView==null)
            {
                LayoutInflater inflador=getLayoutInflater();
                convertView=inflador.inflate(R.layout.categoria, null);
                contenidor=new ContenidorProvincia();
                contenidor.tvProvincia= (TextView) convertView.findViewById(R.id.tvCategoria);
                convertView.setTag(contenidor);
            }
            else
            {
                contenidor= (ContenidorProvincia) convertView.getTag();
            }
            contenidor.tvProvincia.setText(claus[groupPosition]);
            return convertView;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
            ContenidorMunicipi contenidor;
            final ViewGroup parent2=parent;
            if(convertView==null)
            {
                LayoutInflater inflador=getLayoutInflater();
                convertView=inflador.inflate(R.layout.categoria, null);
                contenidor=new ContenidorMunicipi();
                contenidor.tvMunicipi= (TextView) convertView.findViewById(R.id.tvCategoria);
                convertView.setTag(contenidor);
            }
            else
            {
                contenidor= (ContenidorMunicipi) convertView.getTag();

            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ExpandableAdapter.this, "test", Toast.LENGTH_SHORT).show();
                    ArrayList<Producte> productePassar = new ArrayList<Producte>();

                    productePassar.add((Producte) getChild(groupPosition,childPosition));
                    pasarProducte.putParcelableArrayListExtra(PRODUCTE, productePassar);
                    startActivity(pasarProducte);
                }
            });
            Producte p= (Producte) getChild(groupPosition, childPosition);
            contenidor.tvMunicipi.setText(p.getNomProducte());
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false; //ficar true si quan cliquin en algun municipi funcioni
        }
    }
    private class ContenidorProvincia
    {
        TextView tvProvincia;
    }
    private class ContenidorMunicipi
    {
        TextView tvMunicipi;
    }
}
