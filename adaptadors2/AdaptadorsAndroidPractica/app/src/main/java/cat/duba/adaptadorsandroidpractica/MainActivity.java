package cat.duba.adaptadorsandroidpractica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import android.widget.ArrayAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnExecutar;
    Spinner spnOpcio;
    ArrayList<Producte> productes;
    final String[] dades={"Array Adapter", "Base Adapter", "Expandable Adapter", "Recyleview adapter"};
    final static String PRODUCTES="Productes";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        productes=new ArrayList<Producte>();
        btnExecutar= (Button) findViewById(R.id.btnExecutar);
        spnOpcio= (Spinner) findViewById(R.id.spnOpcio);
        btnExecutar.setOnClickListener(this);
        spnOpcio.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dades));
        omplirProductes();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.btnExecutar:
                if(spnOpcio.getSelectedItem().toString()=="Array Adapter")
                {
                    intent=new Intent(this, ArrayActivity.class);
                    intent.putParcelableArrayListExtra(PRODUCTES, productes);
                    startActivity(intent);
                }
                else if(spnOpcio.getSelectedItem().toString()=="Base Adapter")
                {
                    intent=new Intent(this, BaseActivity.class);
                    intent.putParcelableArrayListExtra(PRODUCTES, productes);
                    startActivity(intent);
                }
                else if(spnOpcio.getSelectedItem().toString()=="Expandable Adapter")
                {
                    intent=new Intent(this, ExpandableAdapter.class);
                    intent.putParcelableArrayListExtra(PRODUCTES, productes);
                    startActivity(intent);
                }
                else if(spnOpcio.getSelectedItem().toString()=="Recycleview Adapter")
                {
                    intent=new Intent(this, BaseActivity.class);
                    intent.putParcelableArrayListExtra(PRODUCTES, productes);
                    startActivity(intent);
                }
                break;
        }
    }
    private void omplirProductes()
    {
        try
        {
            String linia;
            String[] camps;

            InputStream fitxerRecursos = getResources().openRawResource(R.raw.productos);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRecursos));

            while ((linia = brEntrada.readLine()) != null)
            {
                camps = linia.split(";");
                productes.add(new Producte(Integer.parseInt(camps[0]), camps[1], camps[2], Double.parseDouble(camps[3]), Integer.parseInt(camps[4])));
            }

            brEntrada.close();
        }
        catch (IOException e)
        {
            Log.e("Lectura Productes", "Error en la lectura dels productes");
        }
    }
}