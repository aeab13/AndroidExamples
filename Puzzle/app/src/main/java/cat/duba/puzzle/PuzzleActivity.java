package cat.duba.puzzle;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.Random;

public class PuzzleActivity extends AppCompatActivity implements View.OnClickListener{
    private TableLayout tlPuzzle;
    private TextView[][] matriu;
    private TextView tvMoviments;
    private int files, columnes, moviments=0;
    private final String FILENAME = "records";
    private Point buit =new Point();
    private String nom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle);
        tlPuzzle= (TableLayout) findViewById(R.id.tlPuzzle);
        tvMoviments= (TextView) findViewById(R.id.tvMoviments);
        Inicialitzar();
        }
//Inicialitza el puzzle
    private void Inicialitzar() {
        Intent intent=getIntent();
        nom=intent.getStringExtra("NOM");
        files=intent.getIntExtra("FILES",3);
        columnes=intent.getIntExtra("COLUMNES",3);
        matriu=new TextView[files][columnes];
        buit.x=columnes-1;
        buit.y=files-1;

        int valor=0;
        for(int i=0;i<files;i++) {
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
            for(int j=0;j<columnes;j++) {
                valor++;
                matriu[i][j] = new TextView(this);
                String numero=String.valueOf(valor);
                matriu[i][j].setGravity(Gravity.CENTER);
                matriu[i][j].setText(numero);
                matriu[i][j].setOnClickListener(this);
                matriu[i][j].setTag(new Point(i, j));
                matriu[i][j].setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1));
                tr.addView(matriu[i][j]);
                ColorRG(i,j);
            }
            tlPuzzle.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1));
        }
        matriu[buit.y][buit.x].setText(" ");
        matriu[buit.y][buit.x].setBackgroundColor(Color.parseColor("#FFFFFF"));
        Barrejar();
        Solucionable();

    }
//Barreja el puzzle
    private void Barrejar() {
        String auxiliar;
        int x1, x2, y1, y2;
        int potencia = (int)Math.pow(files * columnes - 1, 2);
        Random r = new Random();
        for (int i = 0; i < potencia; i++)
        {
            x1 = r.nextInt(columnes);
            x2 = r.nextInt(columnes);
            y1 = r.nextInt(files);
            y2 = r.nextInt(files);
            if (matriu[y1][x1] != matriu[buit.y][buit.x] && matriu[y2][x2] != matriu[buit.y][buit.x])
            {
                auxiliar = String.valueOf(matriu[y1][x1].getText());
                matriu[y1][x1].setText(matriu[y2][x2].getText());
                matriu[y2][x2].setText(auxiliar);
                ColorRG(y1, x1);
                ColorRG(y2, x2);
            }
        }
    }
    //Comprova si és solucionable
    private void Solucionable()
    {
        String aux;
        int[] llistaValors = new int[files * columnes - 1];
        int comptador = 0, k = 0;
        for (int i = 0; i < files; i++)
        {
            for (int j = 0; j < columnes; j++)
            {
                if (matriu[i][j] != matriu[buit.y][buit.x])
                llistaValors[k] = Integer.parseInt(String.valueOf(matriu[i][j].getText()));
                k++;
            }
        }
        for (int i = 0; i < llistaValors.length; i++)
        {
            for (int j = i; j < llistaValors.length; j++)
            {
                if (llistaValors[i] > llistaValors[j])
                    comptador++;
            }
        }

        if (comptador == 0 || comptador % 2 == 1)
        {
            aux = String.valueOf(matriu[files - 1][columnes - 2].getText());
            matriu[files - 1][columnes - 2].setText(matriu[files - 1][columnes - 3].getText());
            matriu[files - 1][columnes - 3].setText(aux);
            ColorRG(files - 1, columnes - 2);
            ColorRG(files - 1, columnes - 3);
        }
    }
    //intercambia la posició d'una peça amb l'espai en blanc
    private void Intercambiar(int i, int j)
    {
        String aux;
        aux = String.valueOf(matriu[i][j].getText());
        matriu[i][j].setText(matriu[buit.y][buit.x].getText());
        matriu[buit.y][buit.x].setText(aux);
        matriu[i][j].setBackgroundColor(Color.parseColor("#FFFFFF"));
        buit.y = i;
        buit.x = j;
    }
//detecta quan cliquen en un quadre.
    @Override
    public void onClick(View v) {
        if(true) {
            TextView t = (TextView) v;
            Point a= (Point)v.getTag();
            int i = a.x;
            int j = a.y;
            if ( buit.x == j) {
                if (buit.y > i) {
                    for (int k = buit.y - 1; k >= i; k--) {
                        Intercambiar(k, j);
                        moviments++;
                        ColorRG(buit.y + 1, buit.x);
                    }
                } else if (buit.y < i) {
                    for (int k = buit.y + 1; k <= i; k++) {
                        Intercambiar(k, j);
                        moviments++;

                        ColorRG(buit.y - 1, (int)buit.x);
                    }
                }
            } else if ( buit.y == i) {
                if ( buit.x > j) {
                    for (int k = buit.x - 1; k >= j; k--) {
                        Intercambiar(i, k);
                        moviments++;
                        ColorRG(buit.y, buit.x + 1);
                    }

                } else if (buit.x < j) {
                    for (int k = buit.x + 1; k <= j; k++) {
                        Intercambiar(i, k);
                        moviments++;
                        ColorRG(buit.y, buit.x - 1);
                    }
                }
            }

             tvMoviments.setText(" Moviments: " + moviments);
            if (Correcte())
            {
                try{
                    //guarda els resultats en un arxiu i tanca l'activitat
                    String cadena=nom+";"+String.valueOf(moviments)+";"+files+"x"+columnes+"\n";
                    FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_APPEND);
                    fos.write(cadena.getBytes());
                    fos.close();
                    Toast.makeText(this, "Completat!", Toast.LENGTH_LONG).show();
                    this.finish();;
                }catch(Exception ex)
                {
                    Log.e("Fitxer intern: ", "L'error ha sigut al provar d'escriure al fitxer en memòria interna: " + ex.getMessage());
                }
            }
        }
    }
//utilitza l'estil adequat segons si una peça està ben col
    private void ColorRG(int i, int j)
    {
        if (i * columnes + j + 1 == Integer.parseInt(matriu[i][j].getText().toString()))
        {
            matriu[i][j].setBackgroundResource(R.drawable.fons_verd);
        }
        else
            matriu[i][j].setBackgroundResource(R.drawable.vermell);

    }
    //Comprova si és correcte
    private boolean Correcte()
    {
        int i = 0, j;
        boolean correcte = true;
        while (i < files && correcte)
        {
            j = 0;
            while (j < columnes && correcte)
            {
                if (matriu[i][j] != matriu[buit.y][buit.x])
                correcte = i * columnes + j + 1 == Integer.parseInt(matriu[i][j].getText().toString());
                j++;
            }
            i++;
        }
        return correcte;
    }
}
