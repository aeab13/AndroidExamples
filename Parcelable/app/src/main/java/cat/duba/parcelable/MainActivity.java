package cat.duba.parcelable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView tvNElemens;
    private Button btnCarregaDades;
    private GridView grdDades;
    private ArrayList<Empleat> empleats;
    private ArrayAdapter<Object> adaptador;
    private int nElems;
    final String NELEMS="NumElements";
    final String EMPLEAT="Empleat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvNElemens = (TextView) findViewById(R.id.tvNElements);
        btnCarregaDades= (Button) findViewById(R.id.btnCarregaDades);
        grdDades= (GridView) findViewById(R.id.grdDades);

        btnCarregaDades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                empleats=new ArrayList<Empleat>();
                empleats.add(new Empleat("Xevi", 999999, true));
                empleats.get(0).getSubordinats().add(new Empleat("Joan", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan2", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan3", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan4", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan5", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan6", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan7", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan8", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan9", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan10", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan11", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan12", 10000, false));
                empleats.get(0).getSubordinats().add(new Empleat("Joan13", 10000, false));
                Object[] taula=empleats.get(0).getSubordinats().toArray();
                nElems=empleats.get(0).getSubordinats().size();
                tvNElemens.setText("N elem: " + nElems);
                adaptador=new ArrayAdapter<Object>(MainActivity.this, android.R.layout.simple_list_item_1, taula);
                grdDades.setAdapter(adaptador);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NELEMS, nElems);
        outState.putParcelableArrayList(EMPLEAT, empleats);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        nElems=savedInstanceState.getInt(NELEMS);
        tvNElemens.setText("N elem: " + nElems);

        empleats=savedInstanceState.getParcelableArrayList(EMPLEAT);
        Object[] taula=empleats.get(0).getSubordinats().toArray();

        adaptador=new ArrayAdapter<Object>(MainActivity.this, android.R.layout.simple_list_item_1, taula);
        grdDades.setAdapter(adaptador);
    }


}
