package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Clientes implements Parcelable {
    private String idCliente, nombreCompania, nombreContacto, cargoContacto, direccion, ciudad, region, codpostal, pais, telefono, fax;

    public Clientes() {
    }

    public Clientes(String idCliente, String nombreCompania, String nombreContacto, String cargoContacto, String direccion, String ciudad, String region, String codpostal, String pais, String telefono, String fax) {
        this.idCliente = idCliente;
        this.nombreCompania = nombreCompania;
        this.nombreContacto = nombreContacto;
        this.cargoContacto = cargoContacto;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.region = region;
        this.codpostal = codpostal;
        this.pais = pais;
        this.telefono = telefono;
        this.fax = fax;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCompania() {
        return nombreCompania;
    }

    public void setNombreCompania(String nombreCompania) {
        this.nombreCompania = nombreCompania;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getCargoContacto() {
        return cargoContacto;
    }

    public void setCargoContacto(String cargoContacto) {
        this.cargoContacto = cargoContacto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCodpostal() {
        return codpostal;
    }

    public void setCodpostal(String codpostal) {
        this.codpostal = codpostal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Override
    public String toString() {
        return "Clientes{" +
                "idCliente='" + idCliente + '\'' +
                ", nombreCompania='" + nombreCompania + '\'' +
                ", nombreContacto='" + nombreContacto + '\'' +
                ", cargoContacto='" + cargoContacto + '\'' +
                ", direccion='" + direccion + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", region='" + region + '\'' +
                ", codpostal='" + codpostal + '\'' +
                ", pais='" + pais + '\'' +
                ", telefono='" + telefono + '\'' +
                ", fax='" + fax + '\'' +
                '}';
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idCliente);
        dest.writeString(nombreCompania);
        dest.writeString(nombreContacto);
        dest.writeString(cargoContacto);
        dest.writeString(direccion);
        dest.writeString(ciudad);
        dest.writeString(region);
        dest.writeString(codpostal);
        dest.writeString(pais);
        dest.writeString(telefono);
        dest.writeString(fax);
    }

    public void readFromParcel (Parcel dades)
    {
        idCliente = dades.readString();
        nombreCompania = dades.readString();
        nombreContacto = dades.readString();
        cargoContacto = dades.readString();
        direccion = dades.readString();
        ciudad = dades.readString();
        region = dades.readString();
        codpostal = dades.readString();
        pais = dades.readString();
        telefono = dades.readString();
        fax = dades.readString();
    }
    public Clientes(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<Clientes> CREATOR = new Creator<Clientes>() {
        @Override
        public Clientes createFromParcel(Parcel source) {
            return new Clientes(source);
        }

        @Override
        public Clientes[] newArray(int size) {
            return new Clientes[size];
        }
    };
}
