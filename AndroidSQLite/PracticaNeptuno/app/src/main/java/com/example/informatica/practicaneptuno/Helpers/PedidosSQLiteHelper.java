package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class PedidosSQLiteHelper extends SQLiteOpenHelper {
    /*
        int idPedido, idCliente, idEmpleado;
    String fechaPedido, fechaEnvio, formaEnvio, destinatario, direccionDestintatario, ciudadDestinatario, regionDestinatario, paisDestintatario;
    double cargo;
    */

    String sqlCreatePedidos= "CREATE TABLE " + Constants.TAULA_PEDIDOS + " (" + Constants.COLUMNA_ID + " integer primary key autoincrement, "
            + Constants.PEDIDOS_ID_PEDIDO + " integer not null, " + Constants.PEDIDOS_ID_CLIENTE + " integer, " + Constants.PEDIDOS_ID_EMPLEADO + " integer, "
            + Constants.PEDIDOS_FECHA_PEDIDO + " TEXT, " + Constants.PEDIDOS_FECHA_ENTREGA + " TEXT, " + Constants.PEDIDOS_FECHA_ENVIO + " TEXT, "
            + Constants.PEDIDOS_FORMA_ENVIO+ " TEXT, " + Constants.PEDIDOS_CARGO + " REAL, " + Constants.PEDIDOS_DESTINATARIO + " TEXT, "+ Constants.PEDIDOS_DIRECCION_DESTINATARIO
            + " TEXT, "+ Constants.PEDIDOS_CIUDAD_DESTINATARIO + " TEXT, "+ Constants.PEDIDOS_REGION_DESTINATARIO + " TEXT, " + Constants.PEDIDOS_CODPOSTAL_DESTINATARIO + " TEXT, " + Constants.PEDIDOS_PAIS_DESTINATARIO + " TEXT);";


    public PedidosSQLiteHelper(Context context) {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public PedidosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public PedidosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreatePedidos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}