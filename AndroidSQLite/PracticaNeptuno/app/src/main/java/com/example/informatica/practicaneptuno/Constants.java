package com.example.informatica.practicaneptuno;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Constants
{
    public final static String NOM_DB = "BBDDAndroid";
    public final static int VERSIO_DB = 1;
    public static final String COLUMNA_ID="_id";

    public static final String TAULA_PROVEEDORES="proveedores",
            PROVEEDORES_ID_PROVEEDOR="id_proveedor",
            PROVEEDORES_NOMBRE_COMPANIA="nombre_compania",
            PROVEEDORES_NOMBRE_CONTACTO="nombre_contacto",
            PROVEEDORES_CARGO_CONTACTO="cargo_contacto",
            PROVEEDORES_DIRECCION="direccion",
            PROVEEDORES_CIUDAD="ciudad",
            PROVEEDORES_REGION="region",
            PROVEEDORES_COD_POSTAL="codpostal",
            PROVEEDORES_PAIS="pais",
            PROVEEDORES_TELEFONO="telefono",
            PROVEEDORES_FAX="fax",
            PROVEEDORES_PAGINA_PRINCIPAL="pagina_principal";
    public static final String
            TAULA_CATEGORIAS="categorias",
            CATEGORIAS_ID_CATEGORIA="id_categoria",
            CATEGORIAS_NOMBRE_CATEGORIA="nombre_categoria",
            CATEGORIAS_DESCRIPCION="descripcion",
            CATEGORIAS_IMAGEN="imagen";
    public static final String
            TAULA_PRODUCTOS="productos",
            PRODUCTOS_ID_PRODUCTO="id_producto",
            PRODUCTOS_NOMBRE_PRODUCTO="nombre_producto",
            PRODUCTOS_ID_PROVEEDOR="id_proveedor",
            PRODUCTOS_ID_CATEGORIA="id_categoria",
            PRODUCTOS_CANTIDAD_POR_UNIDAD="cantidad_por_unidad",
            PRODUCTOS_PRECIO_UNIDAD="precio_unidad",
            PRODUCTOS_UNIDADES_EN_EXISTENCIA="unidades_en_existencia",
            PRODUCTOS_UNIDADES_EN_PEDIDO="unidades_en_pedido",
            PRODUCTOS_NIVEL_NUEVO_PEDIDO="nivel_nuevo_pedido",
            PRODUCTOS_SUSPENDIDO="suspendido";
    public static final String
            TAULA_EMPLEADOS="empleados",
            EMPLEADOS_ID_EMPLEADO="id_empleado",
            EMPLEADOS_APELLIDOS="apellidos",
            EMPLEADOS_NOMBRE="nombre",
            EMPLEADOS_CARGO="cargo",
            EMPLEADOS_TRATAMIENTO="tratamiento",
            EMPLEADOS_FECHA_NACIMIENTO="fecha_nacimiento",
            EMPLEADOS_FECHA_CONTRATACION="fecha_contratacion",
            EMPLEADOS_DIRECCION="direccion",
            EMPLEADOS_CIUDAD="ciudad",
            EMPLEADOS_REGION="region",
            EMPLEADOS_COD_POSTAL="codpostal",
            EMPLEADOS_PAIS="pais",
            EMPLEADOS_TEL_DOMICILIO="tel_domicilio",
            EMPLEADOS_EXTENSION="extension",
            EMPLEADOS_FOTO="foto",
            EMPLEADOS_NOTAS="notas",
            EMPLEADOS_JEFE="jefe";

    public static final String TAULA_CLIENTES="clientes",
            CLIENTES_ID_CLIENTES="id_cliente",
            CLIENTES_NOMBRE_COMPANIA="nombre_compania",
            CLIENTES_NOMBRE_CONTACTO="nombre_contacto",
            CLIENTES_CARGO_CONTACTO="cargo_contacto",
            CLIENTES_DIRECCION="direccion",
            CLIENTES_CIUDAD="ciudad",
            CLIENTES_REGION="region",
            CLIENTES_CODPOSTAL="codpostal",
            CLIENTES_PAIS="pais",
            CLIENTES_TELEFONO="telefono",
            CLIENTES_FAX="fax";

    public static final String TAULA_COMPANIAS_DE_ENVIO="companias_de_envio",
            COMPANIAS_ID_COMPANIA="idCompaniaEnvio",
            COMPANIAS_NOMBRE_COMPANIA="nombre_compania",
            COMPANIAS_TELEFONO="telefono";

    public static final String TAULA_DETALLES_PEDIDO="detalles_pedido",
            DETALLES_ID_PEDIDO="id_pedido",
            DETALLES_ID_PRODUCTO="id_producto",
            DETALLES_CANTIDAD="cantidad",
            DETALLES_PRECIO_UNIDAD="precio_unidad",
            DETALLES_DESCUENTO="descuento";

    public static final String TAULA_PEDIDOS="pedidos",
            PEDIDOS_ID_PEDIDO="id_pedido",
            PEDIDOS_ID_CLIENTE="id_cliente",
            PEDIDOS_ID_EMPLEADO="id_empleado",
            PEDIDOS_FECHA_PEDIDO="fecha_pedido",
            PEDIDOS_FECHA_ENTREGA="fecha_entrega",
            PEDIDOS_FECHA_ENVIO="fecha_envio",
            PEDIDOS_FORMA_ENVIO="forma_envio",
            PEDIDOS_CARGO="cargo",
            PEDIDOS_DESTINATARIO="destinatario",
            PEDIDOS_DIRECCION_DESTINATARIO="direccion_destinatario",
            PEDIDOS_CIUDAD_DESTINATARIO="ciudad_destinatario",
            PEDIDOS_REGION_DESTINATARIO="region_destinatario",
            PEDIDOS_CODPOSTAL_DESTINATARIO="codpostal_destinatario",
            PEDIDOS_PAIS_DESTINATARIO="pais_destinatario";

}