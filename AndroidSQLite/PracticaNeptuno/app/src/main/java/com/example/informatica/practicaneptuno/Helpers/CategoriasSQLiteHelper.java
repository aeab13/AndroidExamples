package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class CategoriasSQLiteHelper extends SQLiteOpenHelper {
    String sqlCreateCategorias="CREATE TABLE "+ Constants.TAULA_CATEGORIAS+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.CATEGORIAS_ID_CATEGORIA+" integer not null, "+ Constants.CATEGORIAS_NOMBRE_CATEGORIA+" TEXT, "+ Constants.CATEGORIAS_DESCRIPCION+" TEXT, "
            + Constants.CATEGORIAS_IMAGEN+" TEXT);";

    public CategoriasSQLiteHelper(Context context)
    {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public CategoriasSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public CategoriasSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateCategorias);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}