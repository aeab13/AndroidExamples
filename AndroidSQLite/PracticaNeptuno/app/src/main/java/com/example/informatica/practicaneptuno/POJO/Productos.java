package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Productos implements Parcelable
{
    private int idProducto;
    private String nombreProducto;
    private int idProveedor;
    private int idCategoria;
    private String cantidadPorUnidad;
    private double precioUnidad;
    private int unidadesEnExistencia;
    private int unidadesEnPedido;
    private int nivelNuevoPedido;
    private int suspendido;

    public Productos() {
    }

    public Productos(int idProducto, String nombreProducto, int idProveedor, int idCategoria, String cantidadPorUnidad, double precioUnidad, int unidadesEnExistencia, int unidadesEnPedido, int nivelNuevoPedido, int suspendido) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.idProveedor = idProveedor;
        this.idCategoria = idCategoria;
        this.cantidadPorUnidad = cantidadPorUnidad;
        this.precioUnidad = precioUnidad;
        this.unidadesEnExistencia = unidadesEnExistencia;
        this.unidadesEnPedido = unidadesEnPedido;
        this.nivelNuevoPedido = nivelNuevoPedido;
        this.suspendido = suspendido;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCantidadPorUnidad() {
        return cantidadPorUnidad;
    }

    public void setCantidadPorUnidad(String cantidadPorUnidad) {
        this.cantidadPorUnidad = cantidadPorUnidad;
    }

    public double getPrecioUnidad() {
        return precioUnidad;
    }

    public void setPrecioUnidad(double precioUnidad) {
        this.precioUnidad = precioUnidad;
    }

    public int getUnidadesEnExistencia() {
        return unidadesEnExistencia;
    }

    public void setUnidadesEnExistencia(int unidadesEnExistencia) {
        this.unidadesEnExistencia = unidadesEnExistencia;
    }

    public int getUnidadesEnPedido() {
        return unidadesEnPedido;
    }

    public void setUnidadesEnPedido(int unidadesEnPedido) {
        this.unidadesEnPedido = unidadesEnPedido;
    }

    public int getNivelNuevoPedido() {
        return nivelNuevoPedido;
    }

    public void setNivelNuevoPedido(int nivelNuevoPedido) {
        this.nivelNuevoPedido = nivelNuevoPedido;
    }

    public int getSuspendido() {
        return suspendido;
    }

    public void setSuspendido(int suspendido) {
        this.suspendido = suspendido;
    }

    @Override
    public String toString() {
        return "Productos{" +
                "idProducto=" + idProducto +
                ", nombreProducto='" + nombreProducto + '\'' +
                ", idProveedor=" + idProveedor +
                ", idCategoria=" + idCategoria +
                ", cantidadPorUnidad='" + cantidadPorUnidad + '\'' +
                ", precioUnidad=" + precioUnidad +
                ", unidadesEnExistencia=" + unidadesEnExistencia +
                ", unidadesEnPedido=" + unidadesEnPedido +
                ", nivelNuevoPedido=" + nivelNuevoPedido +
                ", suspendido=" + suspendido +
                '}';
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idProducto);
        dest.writeString(nombreProducto);
        dest.writeInt(idProveedor);
        dest.writeInt(idCategoria);
        dest.writeString(cantidadPorUnidad);
        dest.writeDouble(precioUnidad);
        dest.writeInt(unidadesEnExistencia);
        dest.writeInt(unidadesEnPedido);
        dest.writeInt(nivelNuevoPedido);
        dest.writeInt(suspendido);
    }

    public void readFromParcel (Parcel dades)
    {
        idProducto = dades.readInt();
        nombreProducto = dades.readString();
        idProveedor = dades.readInt();
        idCategoria = dades.readInt();
        cantidadPorUnidad = dades.readString();
        precioUnidad = dades.readDouble();
        unidadesEnExistencia = dades.readInt();
        unidadesEnPedido = dades.readInt();
        nivelNuevoPedido = dades.readInt();
        suspendido = dades.readInt();
    }

    public Productos(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<Productos> CREATOR = new Parcelable.Creator<Productos>() {
        @Override
        public Productos createFromParcel(Parcel source) {
            return new Productos(source);
        }

        @Override
        public Productos[] newArray(int size) {
            return new Productos[size];
        }
    };
}