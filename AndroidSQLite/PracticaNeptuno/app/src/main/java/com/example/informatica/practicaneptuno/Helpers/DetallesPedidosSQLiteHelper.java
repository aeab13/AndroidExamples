package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class DetallesPedidosSQLiteHelper extends SQLiteOpenHelper {
    String sqlCreateClientes = "CREATE TABLE " + Constants.TAULA_DETALLES_PEDIDO + " (" + Constants.COLUMNA_ID + " integer primary key autoincrement, "
            + Constants.DETALLES_ID_PEDIDO + " integer not null, " + Constants.DETALLES_ID_PRODUCTO + " integer not null, " + Constants.DETALLES_CANTIDAD + " integer, "
            + Constants.DETALLES_PRECIO_UNIDAD + " REAL, " + Constants.DETALLES_DESCUENTO + " REAL);";

    public DetallesPedidosSQLiteHelper(Context context) {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public DetallesPedidosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DetallesPedidosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateClientes);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
