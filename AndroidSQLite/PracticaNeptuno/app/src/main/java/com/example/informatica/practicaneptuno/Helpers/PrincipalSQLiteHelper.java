package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class PrincipalSQLiteHelper extends SQLiteOpenHelper {
    String sqlCreateCategorias="CREATE TABLE "+ Constants.TAULA_CATEGORIAS+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.CATEGORIAS_ID_CATEGORIA+" integer not null, "+ Constants.CATEGORIAS_NOMBRE_CATEGORIA+" TEXT, "+ Constants.CATEGORIAS_DESCRIPCION+" TEXT, "
            + Constants.CATEGORIAS_IMAGEN+" TEXT);";
    String sqlCreateClientes="CREATE TABLE "+ Constants.TAULA_CLIENTES+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.CLIENTES_ID_CLIENTES+" TEXT not null, "+ Constants.CLIENTES_NOMBRE_COMPANIA+" TEXT, "+ Constants.CLIENTES_NOMBRE_CONTACTO+" TEXT, "
            + Constants.CLIENTES_CARGO_CONTACTO+" TEXT, "+ Constants.CLIENTES_DIRECCION+" TEXT, "+ Constants.CLIENTES_CIUDAD+" TEXT, "+ Constants.CLIENTES_REGION+" TEXT, "+Constants.CLIENTES_CODPOSTAL+" TEXT, "+ Constants.CLIENTES_PAIS+" TEXT, "+ Constants.CLIENTES_TELEFONO+" TEXT, "+ Constants.CLIENTES_FAX+" TEXT);";
    String sqlCreateCompaniasEnvio="CREATE TABLE "+ Constants.TAULA_COMPANIAS_DE_ENVIO+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.COMPANIAS_ID_COMPANIA+" integer not null, "+ Constants.COMPANIAS_NOMBRE_COMPANIA+" TEXT, "+ Constants.COMPANIAS_TELEFONO+" TEXT);";
    String sqlCreateDetallesPedido= "CREATE TABLE " + Constants.TAULA_DETALLES_PEDIDO + " (" + Constants.COLUMNA_ID + " integer primary key autoincrement, "
            + Constants.DETALLES_ID_PEDIDO + " integer not null, " + Constants.DETALLES_ID_PRODUCTO + " integer not null, " + Constants.DETALLES_CANTIDAD + " integer, "
            + Constants.DETALLES_PRECIO_UNIDAD + " REAL, " + Constants.DETALLES_DESCUENTO + " REAL);";
    String sqlCreateEmpleados="CREATE TABLE "+ Constants.TAULA_EMPLEADOS+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.EMPLEADOS_ID_EMPLEADO+" integer not null, "+ Constants.EMPLEADOS_APELLIDOS+" TEXT, "+ Constants.EMPLEADOS_NOMBRE+" TEXT, "
            + Constants.EMPLEADOS_CARGO+" TEXT, "+ Constants.EMPLEADOS_TRATAMIENTO+" TEXT, " + Constants.EMPLEADOS_FECHA_NACIMIENTO+ " TEXT, " + Constants.EMPLEADOS_FECHA_CONTRATACION+" TEXT, "+ Constants.EMPLEADOS_DIRECCION+" TEXT, "+ Constants.EMPLEADOS_CIUDAD+" TEXT, "+ Constants.EMPLEADOS_REGION+" TEXT, "+ Constants.EMPLEADOS_COD_POSTAL+" TEXT, " +
            Constants.EMPLEADOS_PAIS+" TEXT, " + Constants.EMPLEADOS_TEL_DOMICILIO + " TEXT, " + Constants.EMPLEADOS_EXTENSION + " TEXT, " + Constants.EMPLEADOS_FOTO + " TEXT, " + Constants.EMPLEADOS_NOTAS + " TEXT, " + Constants.EMPLEADOS_JEFE + " integer);";
    String sqlCreatePedidos= "CREATE TABLE " + Constants.TAULA_PEDIDOS + " (" + Constants.COLUMNA_ID + " integer primary key autoincrement, "
            + Constants.PEDIDOS_ID_PEDIDO + " integer not null, " + Constants.PEDIDOS_ID_CLIENTE + " integer, " + Constants.PEDIDOS_ID_EMPLEADO + " integer, "
            + Constants.PEDIDOS_FECHA_PEDIDO + " TEXT, " + Constants.PEDIDOS_FECHA_ENTREGA + " TEXT, " + Constants.PEDIDOS_FECHA_ENVIO + " TEXT, "
            + Constants.PEDIDOS_FORMA_ENVIO+ " TEXT, " + Constants.PEDIDOS_CARGO + " REAL, " + Constants.PEDIDOS_DESTINATARIO + " TEXT, "+ Constants.PEDIDOS_DIRECCION_DESTINATARIO
            + " TEXT, "+ Constants.PEDIDOS_CIUDAD_DESTINATARIO + " TEXT, "+ Constants.PEDIDOS_REGION_DESTINATARIO + " TEXT, " + Constants.PEDIDOS_CODPOSTAL_DESTINATARIO + " TEXT, " + Constants.PEDIDOS_PAIS_DESTINATARIO + " TEXT);";
    String sqlCreateProductos="CREATE TABLE "+ Constants.TAULA_PRODUCTOS+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.PRODUCTOS_ID_PRODUCTO+" integer not null, "+ Constants.PRODUCTOS_NOMBRE_PRODUCTO+" TEXT, "+ Constants.PRODUCTOS_ID_PROVEEDOR+" integer not null, "
            + Constants.PRODUCTOS_ID_CATEGORIA+" integer not null, " + Constants.PRODUCTOS_CANTIDAD_POR_UNIDAD + " TEXT," + Constants.PRODUCTOS_PRECIO_UNIDAD + " REAL," + Constants.PRODUCTOS_UNIDADES_EN_EXISTENCIA + " integer," +
            Constants.PRODUCTOS_UNIDADES_EN_PEDIDO + " integer," + Constants.PRODUCTOS_NIVEL_NUEVO_PEDIDO + " integer," + Constants.PRODUCTOS_SUSPENDIDO + " integer);";
    String sqlCreateProveedores="CREATE TABLE "+ Constants.TAULA_PROVEEDORES+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.PROVEEDORES_ID_PROVEEDOR+" integer not null, "+ Constants.PROVEEDORES_NOMBRE_COMPANIA+" TEXT, "+ Constants.PROVEEDORES_NOMBRE_CONTACTO+" TEXT, "
            + Constants.PROVEEDORES_CARGO_CONTACTO+" TEXT, "+ Constants.PROVEEDORES_DIRECCION+" TEXT, " + Constants.PROVEEDORES_CIUDAD+ " TEXT, " + Constants.PROVEEDORES_REGION+" TEXT, "+ Constants.PROVEEDORES_COD_POSTAL+" TEXT, "+ Constants.PROVEEDORES_PAIS+" TEXT, "+ Constants.PROVEEDORES_TELEFONO+" TEXT, "+ Constants.PROVEEDORES_FAX+" TEXT, " +
            Constants.PROVEEDORES_PAGINA_PRINCIPAL+" TEXT);";

    public PrincipalSQLiteHelper(Context context) {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public PrincipalSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public PrincipalSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateCategorias);
        db.execSQL(sqlCreateClientes);
        db.execSQL(sqlCreateCompaniasEnvio);
        db.execSQL(sqlCreateDetallesPedido);
        db.execSQL(sqlCreateEmpleados);
        db.execSQL(sqlCreatePedidos);
        db.execSQL(sqlCreateProductos);
        db.execSQL(sqlCreateProveedores);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
