package com.example.informatica.practicaneptuno;

import android.content.Context;
import android.util.Log;

import com.example.informatica.practicaneptuno.POJO.Categorias;
import com.example.informatica.practicaneptuno.POJO.Clientes;
import com.example.informatica.practicaneptuno.POJO.CompaniasDeEnvio;
import com.example.informatica.practicaneptuno.POJO.DetallesDePedidos;
import com.example.informatica.practicaneptuno.POJO.Empleados;
import com.example.informatica.practicaneptuno.POJO.Pedidos;
import com.example.informatica.practicaneptuno.POJO.Productos;
import com.example.informatica.practicaneptuno.POJO.Proveedores;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Informatica on 28/03/2016.
 */
public class Fitxers
{
    public static ArrayList<Clientes> obtenerClientes(Context context)
    {
        ArrayList<Clientes> clientes = new ArrayList<Clientes>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.clientes);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                clientes.add(new Clientes(camps[0], camps[1], camps[2], camps[3], camps[4], camps[5], camps[6], camps[7], camps[8], camps[9], camps[10]));

            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR CLIENTES", ex.getMessage());
        }

        return clientes;
    }
    public static ArrayList<Categorias> obtenerCategorias(Context context)
    {
        ArrayList<Categorias> categorias = new ArrayList<Categorias>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.categorias);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                categorias.add(new Categorias(Integer.parseInt(camps[0]), camps[1], camps[2], camps[3]));
//"IdCategor�a";"NombreCategor�a";"Descripci�n";"Imagen"

            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR CATEGORIAS" , ex.getMessage());
        }

        return categorias;
    }
    public static ArrayList<CompaniasDeEnvio> obtenerCompaniasDeEnvio(Context context)
    {
        ArrayList<CompaniasDeEnvio> companiasDeEnvio = new ArrayList<CompaniasDeEnvio>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.companias);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                companiasDeEnvio.add(new CompaniasDeEnvio(Integer.parseInt(camps[0]), camps[1], camps[2]));
//"IdCategor�a";"NombreCategor�a";"Descripci�n";"Imagen"

            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR COMPANIAS" , ex.getMessage());
        }

        return companiasDeEnvio;
    }
    public static ArrayList<DetallesDePedidos> obtenerDetallesDePedidos(Context context)
    {
        ArrayList<DetallesDePedidos> detallesDePedidos = new ArrayList<DetallesDePedidos>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.detalles);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                detallesDePedidos.add(new DetallesDePedidos(Integer.parseInt(camps[0]), Integer.parseInt(camps[1]), Integer.parseInt(camps[2]), Double.parseDouble(camps[3].substring(0, camps[3].length() - 1)), Double.parseDouble(camps[4])));
                //"IdCompa��aEnv�os";"NombreCompa��a";"Tel�fono"

            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR DETALLES" , ex.getMessage());
        }

        return detallesDePedidos;
    }
    public static ArrayList<Empleados> obtenerEmpleados(Context context)
    {
        ArrayList<Empleados> empleados = new ArrayList<Empleados>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.empleados);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                empleados.add(new Empleados(Integer.parseInt(camps[0]), camps[1], camps[2], camps[3], camps[4], camps[5], camps[6], camps[7], camps[8], camps[9], camps[10], camps[11], camps[12], camps[13], camps[14], camps[15], Integer.parseInt(camps[16])));
//"IdEmpleado";"Apellidos";"Nombre";"Cargo";"Tratamiento";"FechaNacimiento";"FechaContrataci�n";"Direcci�n";"Ciudad";"Regi�n";"C�dPostal";"Pa�s";"TelDomicilio";"Extensi�n";"Foto";"Notas";"Jefe"
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR EMPLEADOS" , ex.getMessage());
        }

        return empleados;
    }
    public static ArrayList<Pedidos> obtenerPedidos(Context context)
    {
        ArrayList<Pedidos> pedidos = new ArrayList<Pedidos>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.pedidos);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                pedidos.add(new Pedidos(Integer.parseInt(camps[0]), Integer.parseInt(camps[1]), Integer.parseInt(camps[2]), camps[3], camps[4], camps[5], camps[6], Double.parseDouble(camps[7].substring(0, camps[7].length())), camps[8], camps[9], camps[10], camps[11], camps[12], camps[13]));
//"IdPedido";"IdCliente";"IdEmpleado";"FechaPedido";"FechaEntrega";"FechaEnv�o";"FormaEnv�o";"Cargo";"Destinatario";"Direcci�nDestinatario";"CiudadDestinatario";"Regi�nDestinatario";"C�dPostalDestinatario";"Pa�sDestinatario"
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR PEDIDOS" , ex.getMessage());
        }

        return pedidos;
    }

    public static ArrayList<Productos> obtenerProductos(Context context)
    {
        ArrayList<Productos> productos = new ArrayList<Productos>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.productos);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                productos.add(new Productos(Integer.parseInt(camps[0]), camps[1], Integer.parseInt(camps[2]), Integer.parseInt(camps[3]), camps[4], Double.parseDouble(camps[5].substring(0, camps[5].length())), Integer.parseInt(camps[6]), Integer.parseInt(camps[7]), Integer.parseInt(camps[8]), Integer.parseInt(camps[9])));
                //"IdProducto";"NombreProducto";"IdProveedor";"IdCategor�a";"CantidadPorUnidad";"PrecioUnidad";"UnidadesEnExistencia";"UnidadesEnPedido";"NivelNuevoPedido";"Suspendido"
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR PRODUCTOS" , ex.getMessage());
        }

        return productos;
    }

    public static ArrayList<Proveedores> obtenerProveedores(Context context)
    {
        ArrayList<Proveedores> proveedores = new ArrayList<Proveedores>();

        try {
            String linia;
            String[] camps;
            InputStream fitxerRaw =  context.getResources().openRawResource(R.raw.proveedores);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRaw));

            while ((linia = brEntrada.readLine()) != null) {
                camps = linia.split(";");
                proveedores.add(new Proveedores(Integer.parseInt(camps[0]), camps[1], camps[2], camps[3], camps[4], camps[5], camps[6], camps[7], camps[8], camps[9], camps[10], camps[11]));
                //   "IdProveedor";"NombreCompa��a";"NombreContacto";"CargoContacto";"Direcci�n";"Ciudad";"Regi�n";"C�dPostal";"Pa�s";"Tel�fono";"Fax";"P�ginaPrincipal"
            }
            fitxerRaw.close();
        } catch (Exception ex) {
            Log.e("·ERROR PROVEEDORES" , ex.getMessage());
        }

        return proveedores;
    }
}