package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.ProductosSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Productos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class ProductosDAO
{
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String [] columnes =
            {
                    Constants.PRODUCTOS_ID_PRODUCTO,
                    Constants.PRODUCTOS_NOMBRE_PRODUCTO,
                    Constants.PRODUCTOS_ID_PROVEEDOR,
                    Constants.PRODUCTOS_ID_CATEGORIA,
                    Constants.PRODUCTOS_CANTIDAD_POR_UNIDAD,
                    Constants.PRODUCTOS_PRECIO_UNIDAD,
                    Constants.PRODUCTOS_UNIDADES_EN_EXISTENCIA,
                    Constants.PRODUCTOS_UNIDADES_EN_PEDIDO,
                    Constants.PRODUCTOS_NIVEL_NUEVO_PEDIDO,
                    Constants.PRODUCTOS_SUSPENDIDO,
            };

    public ProductosDAO(Context context)
    {
        bdHelper = new ProductosSQLiteHelper(context);
    }

    public void obre()
    {
        bd = bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Productos cursorAProductos(Cursor cursor)
    {
        return new Productos(cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getInt(4), cursor.getString(5),
                cursor.getDouble(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9), cursor.getInt(10));
    }

    public Productos creaProductos (int idProducto, String nombreProducto, int idProveedor, int idCategoria, String cantidadPorUnidad, double precioUnidad, int unidadesEnExistencia, int unidadesEnPedido, int nivelNuevoPedido, int suspendido)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.PRODUCTOS_ID_PRODUCTO, idProducto);
        valors.put(Constants.PRODUCTOS_NOMBRE_PRODUCTO, nombreProducto);
        valors.put(Constants.PRODUCTOS_ID_PROVEEDOR, idProveedor);
        valors.put(Constants.PRODUCTOS_ID_CATEGORIA, idCategoria);
        valors.put(Constants.PRODUCTOS_CANTIDAD_POR_UNIDAD, cantidadPorUnidad);
        valors.put(Constants.PRODUCTOS_PRECIO_UNIDAD, precioUnidad);
        valors.put(Constants.PRODUCTOS_UNIDADES_EN_EXISTENCIA, unidadesEnExistencia);
        valors.put(Constants.PRODUCTOS_UNIDADES_EN_PEDIDO, unidadesEnPedido);
        valors.put(Constants.PRODUCTOS_NIVEL_NUEVO_PEDIDO, nivelNuevoPedido);
        valors.put(Constants.PRODUCTOS_SUSPENDIDO, suspendido);


        long idInsercio = bd.insert(
                Constants.TAULA_PRODUCTOS,
                null,
                valors
        );

        if (idInsercio == -1)
        {
            return null;
        }

        else
        {
            Cursor cursor = bd.query(
                    Constants.TAULA_PRODUCTOS,
                    columnes, Constants.PRODUCTOS_ID_PRODUCTO + " = '" + idProducto + "'",
                    null, null, null, null
            );

            cursor.moveToFirst();
            Productos productos = cursorAProductos(cursor);
            cursor.close();

            return productos;
        }
    }

    public boolean eliminaProductos (Productos productos)
    {
        int nEsborrats = bd.delete(Constants.TAULA_PRODUCTOS,
                Constants.PRODUCTOS_ID_PRODUCTO + " = '" + productos.getIdProducto() + "'",
                null);

        return nEsborrats > 0;
    }

    public List<Productos> obteProductos()
    {
        List<Productos> productos = new ArrayList<Productos>();

        Cursor cursor = bd.query(Constants.TAULA_PRODUCTOS,
                columnes,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Productos producto = cursorAProductos(cursor);
            productos.add(producto);
            cursor.moveToNext();
        }

        return productos;
    }
}
