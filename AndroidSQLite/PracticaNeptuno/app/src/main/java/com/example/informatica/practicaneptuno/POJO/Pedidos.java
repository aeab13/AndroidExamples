package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Pedidos implements Parcelable{
    int idPedido, idCliente, idEmpleado;
    String fechaPedido, fechaEntrega, fechaEnvio, formaEnvio, destinatario, direccionDestintatario, ciudadDestinatario, regionDestinatario, codpostalDestinatario ,paisDestintatario;
    double cargo;

    public Pedidos() {
    }

    public Pedidos(int idPedido, int idCliente, int idEmpleado, String fechaPedido, String fechaEntrega, String fechaEnvio, String formaEnvio, double cargo, String destinatario, String direccionDestintatario, String ciudadDestinatario, String regionDestinatario, String codpostalDestinatario, String paisDestintatario) {
        this.idPedido = idPedido;
        this.idCliente = idCliente;
        this.idEmpleado = idEmpleado;
        this.fechaPedido = fechaPedido;
        this.fechaEntrega = fechaEntrega;
        this.fechaEnvio = fechaEnvio;
        this.formaEnvio = formaEnvio;
        this.cargo = cargo;
        this.destinatario = destinatario;
        this.direccionDestintatario = direccionDestintatario;
        this.ciudadDestinatario = ciudadDestinatario;
        this.regionDestinatario = regionDestinatario;
        this.codpostalDestinatario = codpostalDestinatario;
        this.paisDestintatario = paisDestintatario;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getFechaEntrega() {
        return fechaPedido;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega= fechaEntrega;
    }

    public String getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getFormaEnvio() {
        return formaEnvio;
    }

    public void setFormaEnvio(String formaEnvio) {
        this.formaEnvio = formaEnvio;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getDireccionDestintatario() {
        return direccionDestintatario;
    }

    public void setDireccionDestintatario(String direccionDestintatario) {
        this.direccionDestintatario = direccionDestintatario;
    }

    public String getCiudadDestinatario() {
        return ciudadDestinatario;
    }

    public void setCiudadDestinatario(String ciudadDestinatario) {
        this.ciudadDestinatario = ciudadDestinatario;
    }

    public String getRegionDestinatario() {
        return regionDestinatario;
    }

    public void setRegionDestinatario(String regionDestinatario) {
        this.regionDestinatario = regionDestinatario;
    }

    public String getCodpostalDestinatario() {
        return paisDestintatario;
    }

    public void setCodpostalDestinatario(String codpostalDestinatario) {
        this.codpostalDestinatario = codpostalDestinatario;
    }

    public String getPaisDestintatario() {
        return paisDestintatario;
    }

    public void setPaisDestintatario(String paisDestintatario) {
        this.paisDestintatario = paisDestintatario;
    }

    public double getCargo() {
        return cargo;
    }

    public void setCargo(double cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Pedidos{" +
                "idPedido=" + idPedido +
                ", idCliente=" + idCliente +
                ", idEmpleado=" + idEmpleado +
                ", fechaPedido='" + fechaPedido + '\'' +
                ", fechaEntrega='" + fechaEntrega + '\'' +
                ", fechaEnvio='" + fechaEnvio + '\'' +
                ", formaEnvio='" + formaEnvio + '\'' +
                ", cargo='" + cargo + '\'' +
                ", destinatario='" + destinatario + '\'' +
                ", direccionDestintatario='" + direccionDestintatario + '\'' +
                ", ciudadDestinatario='" + ciudadDestinatario + '\'' +
                ", regionDestinatario='" + regionDestinatario + '\'' +
                ", codpostalDestinatario='" + codpostalDestinatario + '\'' +
                ", paisDestintatario='" + paisDestintatario + '\'' +
                '}';
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idPedido);
        dest.writeInt(idCliente);
        dest.writeInt(idEmpleado);
        dest.writeString(fechaPedido);
        dest.writeString(fechaEntrega);
        dest.writeString(fechaEnvio);
        dest.writeString(formaEnvio);
        dest.writeDouble(cargo);
        dest.writeString(destinatario);
        dest.writeString(direccionDestintatario);
        dest.writeString(ciudadDestinatario);
        dest.writeString(regionDestinatario);
        dest.writeString(codpostalDestinatario);
        dest.writeString(paisDestintatario);
    }

    public void readFromParcel (Parcel dades)
    {
        idPedido = dades.readInt();
        idCliente = dades.readInt();
        idEmpleado = dades.readInt();
        fechaPedido = dades.readString();
        fechaEntrega = dades.readString();
        fechaEnvio = dades.readString();
        formaEnvio = dades.readString();
        cargo = dades.readDouble();
        destinatario = dades.readString();
        direccionDestintatario = dades.readString();
        ciudadDestinatario = dades.readString();
        regionDestinatario = dades.readString();
        codpostalDestinatario = dades.readString();
        paisDestintatario = dades.readString();
    }
    public Pedidos(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<Pedidos> CREATOR = new Creator<Pedidos>() {
        @Override
        public Pedidos createFromParcel(Parcel source) {
            return new Pedidos(source);
        }

        @Override
        public Pedidos[] newArray(int size) {
            return new Pedidos[size];
        }
    };
}