package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class ClientesSQLiteHelper extends SQLiteOpenHelper{
    String sqlCreateClientes="CREATE TABLE "+ Constants.TAULA_CLIENTES+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.CLIENTES_ID_CLIENTES+" TEXT not null, "+ Constants.CLIENTES_NOMBRE_COMPANIA+" TEXT, "+ Constants.CLIENTES_NOMBRE_CONTACTO+" TEXT, "
            + Constants.CLIENTES_CARGO_CONTACTO+" TEXT, "+ Constants.CLIENTES_DIRECCION+" TEXT, "+ Constants.CLIENTES_CIUDAD+" TEXT, "+ Constants.CLIENTES_REGION+" TEXT, "+Constants.CLIENTES_CODPOSTAL+" TEXT"+ Constants.CLIENTES_PAIS+" TEXT, "+ Constants.CLIENTES_TELEFONO+" TEXT, "+ Constants.CLIENTES_FAX+" TEXT);";
    public ClientesSQLiteHelper(Context context)
    {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public ClientesSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public ClientesSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateClientes);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
