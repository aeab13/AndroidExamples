package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.EmpleadosSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Empleados;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class EmpleadosDAO
{
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String [] columnes =
            {
                    Constants.EMPLEADOS_ID_EMPLEADO,
                    Constants.EMPLEADOS_APELLIDOS,
                    Constants.EMPLEADOS_NOMBRE,
                    Constants.EMPLEADOS_CARGO,
                    Constants.EMPLEADOS_TRATAMIENTO,
                    Constants.EMPLEADOS_FECHA_NACIMIENTO,
                    Constants.EMPLEADOS_FECHA_CONTRATACION,
                    Constants.EMPLEADOS_DIRECCION,
                    Constants.EMPLEADOS_CIUDAD,
                    Constants.EMPLEADOS_REGION,
                    Constants.EMPLEADOS_COD_POSTAL,
                    Constants.EMPLEADOS_PAIS,
                    Constants.EMPLEADOS_TEL_DOMICILIO,
                    Constants.EMPLEADOS_EXTENSION,
                    Constants.EMPLEADOS_FOTO,
                    Constants.EMPLEADOS_NOTAS,
                    Constants.EMPLEADOS_JEFE,
            };

    public EmpleadosDAO(Context context)
    {
        bdHelper = new EmpleadosSQLiteHelper(context);
    }

    public void obre()
    {
        bd = bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Empleados cursorAEmpleados(Cursor cursor)
    {
        return new Empleados(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10),
                cursor.getString(11), cursor.getString(12), cursor.getString(13), cursor.getString(14), cursor.getString(15),
                cursor.getString(16), cursor.getInt(17));
    }

    public Empleados creaEmpleados (int idEmpleado, String apellidos, String nombre, String cargo, String tratamiento, String fechaNacimiento, String fechaContratacion, String direccion, String ciudad, String region, String codPostal, String pais, String telDomicilio, String extension, String foto, String notas, int jefe)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.EMPLEADOS_ID_EMPLEADO, idEmpleado);
        valors.put(Constants.EMPLEADOS_APELLIDOS, apellidos);
        valors.put(Constants.EMPLEADOS_NOMBRE, nombre);
        valors.put(Constants.EMPLEADOS_CARGO, cargo);
        valors.put(Constants.EMPLEADOS_TRATAMIENTO, tratamiento);
        valors.put(Constants.EMPLEADOS_FECHA_NACIMIENTO, fechaNacimiento);
        valors.put(Constants.EMPLEADOS_FECHA_CONTRATACION, fechaContratacion);
        valors.put(Constants.EMPLEADOS_DIRECCION, direccion);
        valors.put(Constants.EMPLEADOS_CIUDAD, ciudad);
        valors.put(Constants.EMPLEADOS_REGION, region);
        valors.put(Constants.EMPLEADOS_COD_POSTAL, codPostal);
        valors.put(Constants.EMPLEADOS_PAIS, pais);
        valors.put(Constants.EMPLEADOS_TEL_DOMICILIO, telDomicilio);
        valors.put(Constants.EMPLEADOS_EXTENSION, extension);
        valors.put(Constants.EMPLEADOS_FOTO, foto);
        valors.put(Constants.EMPLEADOS_NOTAS, notas);
        valors.put(Constants.EMPLEADOS_JEFE, jefe);


        long idInsercio = bd.insert(
                Constants.TAULA_EMPLEADOS,
                null,
                valors
        );

        if (idInsercio == -1)
        {
            return null;
        }

        else
        {
            Cursor cursor = bd.query(
                    Constants.TAULA_EMPLEADOS,
                    columnes, Constants.EMPLEADOS_ID_EMPLEADO + " = '" + idEmpleado + "'",
                    null, null, null, null
            );

            cursor.moveToFirst();
            Empleados empleados = cursorAEmpleados(cursor);
            cursor.close();

            return empleados;
        }
    }

    public boolean eliminaEmpleados (Empleados empleados)
    {
        int nEsborrats = bd.delete(Constants.TAULA_EMPLEADOS,
                Constants.EMPLEADOS_ID_EMPLEADO + " = '" + empleados.getIdEmpleado() + "'",
                null);

        return nEsborrats > 0;
    }

    public List<Empleados> obteEmpleados()
    {
        List<Empleados> empleados = new ArrayList<Empleados>();

        Cursor cursor = bd.query(Constants.TAULA_EMPLEADOS,
                columnes,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Empleados empleado = cursorAEmpleados(cursor);
            empleados.add(empleado);
            cursor.moveToNext();
        }

        return empleados;
    }
}