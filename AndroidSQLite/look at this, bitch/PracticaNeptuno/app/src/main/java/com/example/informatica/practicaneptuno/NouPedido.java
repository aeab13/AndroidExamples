package com.example.informatica.practicaneptuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.informatica.practicaneptuno.DAO.PedidosDAO;
import com.example.informatica.practicaneptuno.POJO.Pedidos;

public class NouPedido extends AppCompatActivity {
    Button btnGuardar;
    PedidosDAO pedidosDAO;
    int idAnterior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_pedido);
        final Intent intent = getIntent();
        pedidosDAO=new PedidosDAO(this);
        pedidosDAO.obre();
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        final EditText etIdPedido = (EditText) findViewById(R.id.etIdPedido);
        final EditText etIdEmpleado = (EditText) findViewById(R.id.etIdEmpleado);
        final EditText etFechaPedido = (EditText) findViewById(R.id.etFechaPedido);
        final EditText etIdCliente = (EditText) findViewById(R.id.etIdCliente);
        final EditText etFechaEntrega = (EditText) findViewById(R.id.etFechaEntrega);
        final EditText etFechaEnvio = (EditText) findViewById(R.id.etFechaEnvio);
        final EditText etFormaEnvio = (EditText) findViewById(R.id.etFormaEnvio);
        final EditText etDestinatario = (EditText) findViewById(R.id.etDestinatario);
        final EditText etDireccionDestinatario = (EditText) findViewById(R.id.etDireccionDestinatario);
        final EditText etCiudadDestinatario = (EditText) findViewById(R.id.etCiudadDestinatario);
        final EditText etRegionDestinatario = (EditText) findViewById(R.id.etRegionDestinatario);
        final EditText etCodPostalDestinatario = (EditText) findViewById(R.id.etCodPostalDestinatario);
        final EditText etPaisDestinatario = (EditText) findViewById(R.id.etPaisDestinatario);
        final EditText etCargo = (EditText) findViewById(R.id.etCargo);
        if (intent.getBooleanExtra(ComandesActivity.ESMODIFICAT, false)) {
            Pedidos pedidos = intent.getParcelableExtra(ComandesActivity.MODIFICARPEDIDO);
            if (pedidos != null) {
                if (etIdPedido != null) {
                    etIdPedido.setText(String.valueOf(pedidos.getIdPedido()));
                    idAnterior = pedidos.getIdPedido();
                }
                if (etIdEmpleado != null) {
                    etIdEmpleado.setText(String.valueOf(pedidos.getIdEmpleado()));
                }
                if (etFechaPedido != null) {
                    etFechaPedido.setText(pedidos.getFechaPedido());
                }
                if (etIdCliente != null) {
                    etIdCliente.setText(pedidos.getIdCliente());
                }
                if (etFechaEntrega != null) {
                    etFechaEntrega.setText(pedidos.getFechaEntrega());
                }
                if (etFechaEnvio != null) {

                    etFechaEnvio.setText(pedidos.getFechaEnvio());
                }
                if (etFormaEnvio != null) {
                    etFormaEnvio.setText(pedidos.getFechaPedido());
                }
                if (etDestinatario != null) {
                    etDestinatario.setText(pedidos.getDestinatario());
                }
                if (etDireccionDestinatario != null) {
                    etDireccionDestinatario.setText(pedidos.getDireccionDestintatario());
                }
                if (etCiudadDestinatario != null) {
                    etCiudadDestinatario.setText(pedidos.getCiudadDestinatario());
                }
                if (etRegionDestinatario != null) {
                    etRegionDestinatario.setText(pedidos.getRegionDestinatario());
                }
                if (etCodPostalDestinatario != null) {
                    etCodPostalDestinatario.setText(pedidos.getCodpostalDestinatario());
                }
                if (etPaisDestinatario != null) {
                    etPaisDestinatario.setText(pedidos.getPaisDestintatario());
                }
                if (etCargo != null) {
                    etCargo.setText(String.valueOf(pedidos.getCargo()));
                }
            }
        }
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (intent.getBooleanExtra(Proveidors.ESMODIFICAT, false)) {
                    pedidosDAO.updatePedidos(idAnterior, Integer.valueOf(etIdPedido.getText().toString()), etIdCliente.getText().toString(), Integer.valueOf(etIdEmpleado.getText().toString()), etFechaPedido.getText().toString(),
                            etFechaEntrega.getText().toString(), etFechaEnvio.getText().toString(), etFormaEnvio.getText().toString(), Double.valueOf(etCargo.getText().toString()),
                            etDestinatario.getText().toString(), etDireccionDestinatario.getText().toString(), etCiudadDestinatario.getText().toString(), etRegionDestinatario.getText().toString(), etCodPostalDestinatario.getText().toString(), etPaisDestinatario.getText().toString());
                } else {
                    pedidosDAO.creaPedido(Integer.valueOf(etIdPedido.getText().toString()), etIdCliente.getText().toString(), Integer.valueOf(etIdEmpleado.getText().toString()), etFechaPedido.getText().toString(),
                            etFechaEntrega.getText().toString(), etFechaEnvio.getText().toString(), etFormaEnvio.getText().toString(), Double.valueOf(etCargo.getText().toString()),
                            etDestinatario.getText().toString(), etDireccionDestinatario.getText().toString(), etCiudadDestinatario.getText().toString(), etRegionDestinatario.getText().toString(), etCodPostalDestinatario.getText().toString(), etPaisDestinatario.getText().toString());
                }
                finish();
            }

        });
    }
}
