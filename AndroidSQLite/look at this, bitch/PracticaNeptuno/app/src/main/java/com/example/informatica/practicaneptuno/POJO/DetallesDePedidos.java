package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class DetallesDePedidos implements Parcelable
{
    int idPedido, idProducto, cantidad;
    double precioUnidad, descuento;

    public DetallesDePedidos() {
    }

    public DetallesDePedidos(int idPedido, int idProducto,  double precioUnidad, int cantidad, double descuento) {
        this.idPedido = idPedido;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precioUnidad = precioUnidad;
        this.descuento = descuento;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioUnidad() {
        return precioUnidad;
    }

    public void setPrecioUnidad(double precioUnidad) {
        this.precioUnidad = precioUnidad;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idPedido);
        dest.writeInt(idProducto);
        dest.writeInt(cantidad);
        dest.writeDouble(precioUnidad);
        dest.writeDouble(descuento);
    }

    public void readFromParcel (Parcel dades)
    {
        idPedido = dades.readInt();
        idProducto = dades.readInt();
        cantidad = dades.readInt();
        precioUnidad = dades.readDouble();
        descuento = dades.readDouble();
    }

    public DetallesDePedidos(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<DetallesDePedidos> CREATOR = new Creator<DetallesDePedidos>() {
        @Override
        public DetallesDePedidos createFromParcel(Parcel source) {
            return new DetallesDePedidos(source);
        }

        @Override
        public DetallesDePedidos[] newArray(int size) {
            return new DetallesDePedidos[size];
        }
    };

}
