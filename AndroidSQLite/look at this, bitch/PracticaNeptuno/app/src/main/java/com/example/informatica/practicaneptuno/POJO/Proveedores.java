package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Proveedores implements Parcelable
{
    private int idProveedor;
    private String nombreCompania;
    private String nombreContacto;
    private String cargoContacto;
    private String direccion;
    private String ciudad;
    private String region;
    private String codPostal;
    private String pais;
    private String telefono;
    private String fax;
    private String paginaPrincipal;

    public Proveedores() {
    }

    public Proveedores(int idProveedor, String nombreCompania, String nombreContacto, String cargoContacto, String direccion, String ciudad, String region, String codPostal, String pais, String telefono, String fax, String paginaPrincipal) {
        this.idProveedor = idProveedor;
        this.nombreCompania = nombreCompania;
        this.nombreContacto = nombreContacto;
        this.cargoContacto = cargoContacto;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.region = region;
        this.codPostal = codPostal;
        this.pais = pais;
        this.telefono = telefono;
        this.fax = fax;
        this.paginaPrincipal = paginaPrincipal;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombreCompania() {
        return nombreCompania;
    }

    public void setNombreCompania(String nombreCompania) {
        this.nombreCompania = nombreCompania;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getCargoContacto() {
        return cargoContacto;
    }

    public void setCargoContacto(String cargoContacto) {
        this.cargoContacto = cargoContacto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPaginaPrincipal() {
        return paginaPrincipal;
    }

    public void setPaginaPrincipal(String paginaPrincipal) {
        this.paginaPrincipal = paginaPrincipal;
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idProveedor);
        dest.writeString(nombreCompania);
        dest.writeString(nombreContacto);
        dest.writeString(cargoContacto);
        dest.writeString(direccion);
        dest.writeString(ciudad);
        dest.writeString(region);
        dest.writeString(codPostal);
        dest.writeString(pais);
        dest.writeString(telefono);
        dest.writeString(fax);
        dest.writeString(paginaPrincipal);
    }

    public void readFromParcel (Parcel dades)
    {
        idProveedor = dades.readInt();
        nombreCompania = dades.readString();
        nombreContacto = dades.readString();
        cargoContacto = dades.readString();
        direccion = dades.readString();
        ciudad = dades.readString();
        region = dades.readString();
        codPostal = dades.readString();
        pais = dades.readString();
        telefono = dades.readString();
        fax = dades.readString();
        paginaPrincipal = dades.readString();
    }
    public Proveedores(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<Proveedores> CREATOR = new Creator<Proveedores>() {
        @Override
        public Proveedores createFromParcel(Parcel source) {
            return new Proveedores(source);
        }

        @Override
        public Proveedores[] newArray(int size) {
            return new Proveedores[size];
        }
    };

}
