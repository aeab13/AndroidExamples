package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.CompaniasDeEnvio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class CompaniasEnvioDAO
{
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String[] columnes={
            Constants.COLUMNA_ID,
            Constants.COMPANIAS_ID_COMPANIA,
            Constants.COMPANIAS_NOMBRE_COMPANIA,
            Constants.COMPANIAS_TELEFONO
    };

    public CompaniasEnvioDAO(Context context)
    {
        bdHelper=new PrincipalSQLiteHelper(context);
    }
    public void obre()
    {
        bd=bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public CompaniasDeEnvio cursorACompania(Cursor cursor)
    {
        return  new CompaniasDeEnvio(cursor.getInt(1), cursor.getString(2), cursor.getString(3));
    }

    public CompaniasDeEnvio creaCompania(int idCompania, String nombreCompania, String telefono)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.COMPANIAS_ID_COMPANIA, idCompania);
        valors.put(Constants.COMPANIAS_NOMBRE_COMPANIA, nombreCompania);
        valors.put(Constants.COMPANIAS_TELEFONO, telefono);

        long idInsercio = bd.insert(Constants.TAULA_COMPANIAS_DE_ENVIO, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = bd.query(Constants.TAULA_COMPANIAS_DE_ENVIO, columnes, Constants.COMPANIAS_ID_COMPANIA+ " = '" + idCompania + "'",
                    null, null, null, null);

            cursor.moveToFirst();

            CompaniasDeEnvio companiasDeEnvio= cursorACompania(cursor);
            cursor.close();
            return companiasDeEnvio;
        }
    }
    public boolean eliminaCompania(CompaniasDeEnvio compania)
    {
        int nEsborrats=bd.delete(
                Constants.TAULA_CLIENTES,
                Constants.COMPANIAS_ID_COMPANIA+" = '"
                        +compania.getIdCompaniaEnvio()+"'", null);
        return nEsborrats>0;
    }

    public List<CompaniasDeEnvio> obteCompanias()
    {
        List<CompaniasDeEnvio> companias=new ArrayList<CompaniasDeEnvio>();

        Cursor cursor=bd.query(
                Constants.TAULA_COMPANIAS_DE_ENVIO,
                columnes,
                null, null, null, null, null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            CompaniasDeEnvio compania= cursorACompania(cursor);
            companias.add(compania);
            cursor.moveToNext();
        }

        return companias;
    }
}
