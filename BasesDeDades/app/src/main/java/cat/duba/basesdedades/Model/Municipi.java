package cat.duba.basesdedades.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abde on 14/03/16.
 */
public class Municipi implements Parcelable {
    private int codiProvincia;
    private int codiIne;
    private String nomProvincia;
    private String nomMunicipi;

    public Municipi()
    {

    }

    public Municipi(int codiProvincia, int codiIne, String nomProvincia, String nomMunicipi)
    {
        this.setCodiProvincia(codiProvincia);
        this.setCodiIne(codiIne);
        this.setNomMunicipi(nomMunicipi);
        this.setNomProvincia(nomProvincia);
    }

    public int getCodiProvincia() {
        return codiProvincia;
    }

    public void setCodiProvincia(int codiProvincia) {
        this.codiProvincia = codiProvincia;
    }

    public int getCodiIne() {
        return codiIne;
    }

    public void setCodiIne(int codiIne) {
        this.codiIne = codiIne;
    }

    public String getNomProvincia() {
        return nomProvincia;
    }

    public void setNomProvincia(String nomProvincia) {
        this.nomProvincia = nomProvincia;
    }

    public String getNomMunicipi() {
        return nomMunicipi;
    }

    public void setNomMunicipi(String nomMunicipi) {
        this.nomMunicipi = nomMunicipi;
    }

    @Override
    public String toString() {
        return nomMunicipi;
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(codiProvincia);
        dest.writeInt(codiIne);
        dest.writeString(nomProvincia);
        dest.writeString(nomMunicipi);
    }

    public void readFromParcel (Parcel dades)
    {
        codiProvincia = dades.readInt();
        codiIne = dades.readInt();
        nomProvincia = dades.readString();
        nomMunicipi = dades.readString();
    }
    public Municipi(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Creator<Municipi> CREATOR = new Creator<Municipi>() {
        @Override
        public Municipi createFromParcel(Parcel source) {
            return new Municipi(source);
        }

        @Override
        public Municipi[] newArray(int size) {
            return new Municipi[size];
        }
    };
}