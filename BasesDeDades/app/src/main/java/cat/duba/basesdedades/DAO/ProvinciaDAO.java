package cat.duba.basesdedades.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import cat.duba.basesdedades.Model.Provincia;
import cat.duba.basesdedades.MunicipisSQLiteHelper;

/**
 * Created by abde on 15/03/16.
 */
public class ProvinciaDAO {
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String[] columnes={
            MunicipisSQLiteHelper.COLUMNA_ID,
            MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA,
            MunicipisSQLiteHelper.COLUMNA_NOM_PROVINCIA
    };

    public ProvinciaDAO(Context context)
    {
        bdHelper=new MunicipisSQLiteHelper(context);
    }
    public void obre()
    {
        bd=bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Provincia cursorAProvincia(Cursor cursor)
    {
        return  new Provincia(cursor.getInt(1), cursor.getString(2));
    }

    public Provincia creaProvincia(int codiProvincia, String nomProvincia)
    {
        ContentValues valors = new ContentValues();
        valors.put(MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA, codiProvincia);
        valors.put(MunicipisSQLiteHelper.COLUMNA_NOM_PROVINCIA, nomProvincia);

        long idInsercio = bd.insert(MunicipisSQLiteHelper.TAULA_PROVINCIES, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = bd.query(MunicipisSQLiteHelper.TAULA_PROVINCIES, columnes, MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA + " = '" + codiProvincia + "'",
                    null, null, null, null);

            cursor.moveToFirst();

            Provincia provincia = cursorAProvincia(cursor);
            cursor.close();
            return provincia;
        }
    }
    public boolean eliminaProvincia(Provincia provincia)
    {
        int nEsborrats=bd.delete(
                MunicipisSQLiteHelper.TAULA_PROVINCIES,
                MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA+" = '"
                +provincia.getCodiProvincia()+"'", null);
        return nEsborrats>0;
    }

    public List<Provincia> obteProvincies()
    {
        List<Provincia> provincies=new ArrayList<Provincia>();

        Cursor cursor=bd.query(
                MunicipisSQLiteHelper.TAULA_PROVINCIES,
                columnes,
                null, null, null, null, null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Provincia provincia= cursorAProvincia(cursor);
            provincies.add(provincia);
            cursor.moveToNext();
        }

        return  provincies;
    }
}
