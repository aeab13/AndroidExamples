package cat.duba.basesdedades;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by abde on 14/03/16.
 */
public class MunicipisSQLiteHelper extends SQLiteOpenHelper {

    public static final String TAULA_PROVINCIES="provincies", COLUMNA_ID="_id", COLUMNA_ID_PROVINCIA="id_provincia", COLUMNA_NOM_PROVINCIA="provincia";
    public static final String TAULA_MUNICIPIS="municipis", COLUMNA_ID_MUNICIPI="id_municipi", COLUMNA_NOM_MUNICIPI="municipi";

    String sqlCreateProvincies="CREATE TABLE "+ TAULA_PROVINCIES+" ("+COLUMNA_ID+" integer primary key autoincrement, "+ COLUMNA_ID_PROVINCIA+" TEXT not null, "+COLUMNA_NOM_PROVINCIA+" TEXT);";
    String sqlCreateMunicipis="CREATE TABLE "+ TAULA_MUNICIPIS+" ("+COLUMNA_ID+" integer primary key autoincrement, "+ COLUMNA_ID_MUNICIPI+" TEXT not null, "+COLUMNA_ID_PROVINCIA+" TEXT not null, "+COLUMNA_NOM_MUNICIPI+" TEXT);";

    String sqlRelacions="ALTER TABLE "+ TAULA_MUNICIPIS+" add constraint fk_1 FOREIGN KEY ("+COLUMNA_ID_PROVINCIA+") references "+TAULA_PROVINCIES+" ("+COLUMNA_ID_PROVINCIA+");";

    public static final String sqlPragma = "PRAGMA foreign_keys=ON;";
    public final static String NOM_DB = "BBDDMunicipis";
    private final static int VERSIO_DB = 1;


    public MunicipisSQLiteHelper(Context context)
    {
        super(context, NOM_DB, null, VERSIO_DB);
    }

    public MunicipisSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public MunicipisSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateProvincies);
        db.execSQL(sqlCreateMunicipis);
    //    db.execSQL(sqlRelacions);
    //    db.execSQL(sqlPragma);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
