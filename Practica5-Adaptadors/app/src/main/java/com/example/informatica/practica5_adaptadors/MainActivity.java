package com.example.informatica.practica5_adaptadors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Spinner spnArray;
    Button btnExecutar;
    Intent intent;
    ArrayList<Producte> productes;
    final String[] dades={"Array Adapter", "Base Adapter"};

    final static String PRODUCTES = "Productes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spnArray = (Spinner) findViewById(R.id.spnArray);
        btnExecutar = (Button) findViewById(R.id.btnExecutar);
        btnExecutar.setOnClickListener(this);

        spnArray.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                dades));

        productes = new ArrayList<Producte>();

        omplirProductes();
    }

    private void omplirProductes()
    {
        try
        {
            String linia;
            String[] camps;

            InputStream fitxerRecursos = getResources().openRawResource(R.raw.producte);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRecursos));

            while ((linia = brEntrada.readLine()) != null)
            {
                camps = linia.split(";");
                productes.add(new Producte(Integer.parseInt(camps[0]), camps[1], camps[2], Double.parseDouble(camps[3]), Integer.parseInt(camps[4])));
            }

            brEntrada.close();
        }
        catch (IOException e)
        {
            Log.e("Lectura Productes", "Error en la lectura dels productes");
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnExecutar:

                if (spnArray.getSelectedItem().toString() == "Array Adapter")
                {
                    intent = new Intent(this, ArrayActivity.class);
                    intent.putParcelableArrayListExtra(PRODUCTES, productes);
                    startActivity(intent);
                }

                else if (spnArray.getSelectedItem().toString() == "Base Adapter")
                {
                    intent = new Intent(this, BaseActivity.class);
                    intent.putParcelableArrayListExtra(PRODUCTES, productes);
                    startActivity(intent);
                }

                break;
        }
    }
}
