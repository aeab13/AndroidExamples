package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Clientes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class ClientesDAO {
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String[] columnes={
            Constants.COLUMNA_ID,
            Constants.CLIENTES_ID_CLIENTES,
            Constants.CLIENTES_NOMBRE_COMPANIA,
            Constants.CLIENTES_NOMBRE_CONTACTO,
            Constants.CLIENTES_CARGO_CONTACTO,
            Constants.CLIENTES_DIRECCION,
            Constants.CLIENTES_CIUDAD,
            Constants.CLIENTES_REGION,
            Constants.CLIENTES_CODPOSTAL,
            Constants.CLIENTES_PAIS,
            Constants.CLIENTES_TELEFONO,
            Constants.CLIENTES_FAX
    };

    public ClientesDAO(Context context)
    {
        bdHelper=new PrincipalSQLiteHelper(context);
    }
    public void obre()
    {
        bd=bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Clientes cursorACliente(Cursor cursor)
    {
        return  new Clientes(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9),cursor.getString(10),cursor.getString(11));
    }

    public Clientes creaCliente(String idCliente, String nombreCompania, String nombreContacto, String cargoContacto, String direccion, String ciudad, String region, String codpostal, String pais, String telefono, String fax)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.CLIENTES_ID_CLIENTES, idCliente);
        valors.put(Constants.CLIENTES_NOMBRE_COMPANIA, nombreCompania);
        valors.put(Constants.CLIENTES_NOMBRE_CONTACTO, nombreContacto);
        valors.put(Constants.CLIENTES_CARGO_CONTACTO, cargoContacto);
        valors.put(Constants.CLIENTES_DIRECCION, direccion);
        valors.put(Constants.CLIENTES_CIUDAD, ciudad);
        valors.put(Constants.CLIENTES_REGION, region);
        valors.put(Constants.CLIENTES_CODPOSTAL, codpostal);
        valors.put(Constants.CLIENTES_PAIS, pais);
        valors.put(Constants.CLIENTES_TELEFONO, telefono);
        valors.put(Constants.CLIENTES_FAX, fax);

        long idInsercio = bd.insert(Constants.TAULA_CLIENTES, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = bd.query(Constants.TAULA_CLIENTES, columnes, Constants.CLIENTES_ID_CLIENTES+ " = '" + idCliente + "'",
                    null, null, null, null);

            cursor.moveToFirst();

            Clientes cliente = cursorACliente(cursor);
            cursor.close();
            return cliente;
        }
    }
    public boolean eliminaCliente(Clientes cliente)
    {
        int nEsborrats=bd.delete(
                Constants.TAULA_CLIENTES,
                Constants.CLIENTES_ID_CLIENTES+" = '"
                        +cliente.getIdCliente()+"'", null);
        return nEsborrats>0;
    }

    public List<Clientes> obteClientes()
    {
        List<Clientes> clientes=new ArrayList<Clientes>();

        Cursor cursor=bd.query(
                Constants.TAULA_CLIENTES,
                columnes,
                null, null, null, null, null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Clientes cliente= cursorACliente(cursor);
            clientes.add(cliente);
            cursor.moveToNext();
        }

        return  clientes;
    }
}
