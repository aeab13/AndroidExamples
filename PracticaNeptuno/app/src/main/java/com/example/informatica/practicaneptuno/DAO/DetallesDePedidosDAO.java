package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.DetallesDePedidos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class DetallesDePedidosDAO {
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String[] columnes={
            Constants.COLUMNA_ID,
            Constants.DETALLES_ID_PEDIDO,
            Constants.DETALLES_ID_PRODUCTO,
            Constants.DETALLES_CANTIDAD,
            Constants.DETALLES_PRECIO_UNIDAD,
            Constants.DETALLES_DESCUENTO
    };

    public DetallesDePedidosDAO(Context context)
    {
        bdHelper=new PrincipalSQLiteHelper(context);
    }
    public void obre()
    {
        bd=bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public DetallesDePedidos cursorADetalle(Cursor cursor)
    {
        return  new DetallesDePedidos(cursor.getInt(1), cursor.getInt(2), cursor.getDouble(4),cursor.getInt(3),  cursor.getDouble(5));
    }

    public DetallesDePedidos creaDetalles(int idPedido, int idProducto, int cantidad, double precioUnidad, double descuento)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.DETALLES_ID_PEDIDO, idPedido);
        valors.put(Constants.DETALLES_ID_PRODUCTO, idProducto);
        valors.put(Constants.DETALLES_CANTIDAD, cantidad);
        valors.put(Constants.DETALLES_DESCUENTO, descuento);
        valors.put(Constants.DETALLES_PRECIO_UNIDAD, precioUnidad);

        long idInsercio = bd.insert(Constants.TAULA_DETALLES_PEDIDO, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = bd.query(Constants.TAULA_DETALLES_PEDIDO, columnes, Constants.DETALLES_ID_PEDIDO+ " = '" + idPedido + "'",
                    null, null, null, null);

            cursor.moveToFirst();

            DetallesDePedidos detallesDePedidos= cursorADetalle(cursor);
            cursor.close();
            return detallesDePedidos;
        }
    }
    public boolean eliminaDetalle(DetallesDePedidos detallePedido)
    {
        int nEsborrats=bd.delete(
                Constants.TAULA_DETALLES_PEDIDO,
                Constants.DETALLES_ID_PEDIDO+" = '"
                        +detallePedido.getIdPedido()+"'", null);
        return nEsborrats>0;
    }

    public List<DetallesDePedidos> obteDetalles()
    {
        List<DetallesDePedidos> detallesDePedidos=new ArrayList<DetallesDePedidos>();

        Cursor cursor=bd.query(
                Constants.TAULA_DETALLES_PEDIDO,
                columnes,
                null, null, null, null, null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            DetallesDePedidos pedidos= cursorADetalle(cursor);
            detallesDePedidos.add(pedidos);
            cursor.moveToNext();
        }

        return detallesDePedidos;
    }
    public List<DetallesDePedidos> obteDetallesPorPedido(int idPedido)
    {
        List<DetallesDePedidos> detallesDePedidos = new ArrayList<DetallesDePedidos>();

        Cursor cursor=bd.query(Constants.TAULA_DETALLES_PEDIDO, columnes,
                Constants.DETALLES_ID_PEDIDO + " = '" + idPedido + "'",
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            DetallesDePedidos detalle = cursorADetalle(cursor);
            detallesDePedidos.add(detalle);
            cursor.moveToNext();
        }

        return detallesDePedidos;
    }
    public int Total(int id) {
        Cursor mCursor=bd.rawQuery("SELECT SUM("+Constants.DETALLES_PRECIO_UNIDAD+") FROM "+Constants.TAULA_DETALLES_PEDIDO+" WHERE "+Constants.DETALLES_ID_PEDIDO+" = "+id, null);
        mCursor.moveToFirst();
        int total= mCursor.getInt(0);
        mCursor.close();
        return total;
    }
}
