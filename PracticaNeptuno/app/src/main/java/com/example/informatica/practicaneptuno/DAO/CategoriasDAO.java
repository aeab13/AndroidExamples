package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Categorias;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class CategoriasDAO
{
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String [] columnes =
            {
                    Constants.COLUMNA_ID,
                    Constants.CATEGORIAS_ID_CATEGORIA,
                    Constants.CATEGORIAS_NOMBRE_CATEGORIA,
                    Constants.CATEGORIAS_DESCRIPCION,
                    Constants.CATEGORIAS_IMAGEN,
            };

    public CategoriasDAO(Context context)
    {
        bdHelper = new PrincipalSQLiteHelper(context);
    }

    public void obre()
    {
        bd = bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Categorias cursorACategorias(Cursor cursor)
    {
        return new Categorias(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
    }

    public Categorias creaCategorias (int idCategoria, String nombreCategoria, String descripcion, String imagen)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.CATEGORIAS_ID_CATEGORIA, idCategoria);
        valors.put(Constants.CATEGORIAS_NOMBRE_CATEGORIA, nombreCategoria);
        valors.put(Constants.CATEGORIAS_DESCRIPCION, descripcion);
        valors.put(Constants.CATEGORIAS_IMAGEN, imagen);

        long idInsercio = bd.insert(
                Constants.TAULA_CATEGORIAS,
                null,
                valors
        );

        if (idInsercio == -1)
        {
            return null;
        }

        else
        {
            Cursor cursor = bd.query(
                    Constants.TAULA_CATEGORIAS,
                    columnes, Constants.CATEGORIAS_ID_CATEGORIA + " = '" + idCategoria + "'",
                    null, null, null, null
            );

            cursor.moveToFirst();
            Categorias categorias = cursorACategorias(cursor);
            cursor.close();

            return categorias;
        }
    }

    public boolean eliminaCategorias (Categorias categorias)
    {
        int nEsborrats = bd.delete(Constants.TAULA_CATEGORIAS,
                Constants.CATEGORIAS_ID_CATEGORIA + " = '" + categorias.getIdCategoria() + "'",
                null);

        return nEsborrats > 0;
    }

    public List<Categorias> obteCategorias()
    {
        List<Categorias> categorias = new ArrayList<Categorias>();

        Cursor cursor = bd.query(Constants.TAULA_CATEGORIAS,
                columnes,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Categorias categoria = cursorACategorias(cursor);
            categorias.add(categoria);
            cursor.moveToNext();
        }

        return categorias;
    }
}
