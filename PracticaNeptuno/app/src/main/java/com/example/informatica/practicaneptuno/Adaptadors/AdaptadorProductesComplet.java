package com.example.informatica.practicaneptuno.Adaptadors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.POJO.Productos;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 1/04/16.
 */
public class AdaptadorProductesComplet extends BaseAdapter {
    List<Productos> productos;
    Context context;
    LayoutInflater layoutInflater;

    public AdaptadorProductesComplet(List<Productos> productos, Context context) {
        this.productos = productos;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return productos.size();
    }

    @Override
    public Object getItem(int position) {
        return productos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return productos.get(position).getIdProducto();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.productes_complet_view_adapter, null);

        TextView tvNombreProducto = (TextView) view.findViewById(R.id.tvNombreProducto);
        TextView tvSuspendido = (TextView) view.findViewById(R.id.tvSuspendido);
        TextView tvCantidadPorUnidad = (TextView) view.findViewById(R.id.tvCantidadPorUnidad);
        TextView tvPrecioPorUnidad = (TextView) view.findViewById(R.id.tvPrecioPorUnidad);
        TextView tvIdProducto= (TextView) view.findViewById(R.id.tvIdProducto);
        TextView tvIdProveedor= (TextView) view.findViewById(R.id.tvIdProveedor);
        TextView tvIdCategoria= (TextView) view.findViewById(R.id.tvIdCategoria);
        TextView tvUnidadesEnExistencia= (TextView) view.findViewById(R.id.tvUnidadesEnExistencia);
        TextView tvUnidadesEnPedido= (TextView) view.findViewById(R.id.tvUnidadesEnPedido);
        TextView tvNivelNuevoPedido= (TextView) view.findViewById(R.id.tvNivelNuevoPedido);
        /*private int idProducto;
        private int idProveedor;
        private int idCategoria;
        private int unidadesEnExistencia;
        private int unidadesEnPedido;
        private int nivelNuevoPedido;*/
        tvIdProducto.setText("ID: "+String.valueOf(productos.get(position).getIdProducto()));
        tvIdProveedor.setText("ID Proveedor: "+String.valueOf(productos.get(position).getIdProveedor()));
        tvIdCategoria.setText("ID Categoría: "+String.valueOf(productos.get(position).getIdCategoria()));
        tvUnidadesEnExistencia.setText("Unidades en existencia: "+String.valueOf(productos.get(position).getUnidadesEnExistencia()));
        tvUnidadesEnPedido.setText("Unidades en pedido: "+String.valueOf(productos.get(position).getUnidadesEnPedido()));
        tvNivelNuevoPedido.setText("Nivel nuevo pedido: "+String.valueOf(productos.get(position).getNivelNuevoPedido()));
        tvNombreProducto.setText("Nombre producto: "+String.valueOf(productos.get(position).getNombreProducto()));
        tvCantidadPorUnidad.setText("Cantidad por unidad: "+String.valueOf(productos.get(position).getCantidadPorUnidad()));
        tvPrecioPorUnidad.setText("Precio por unidad: "+String.valueOf(productos.get(position).getPrecioUnidad()));
        if (productos.get(position).getSuspendido() == 1) {
            tvSuspendido.setText("SUSPENDIDO");
        } else {
            tvSuspendido.setText("NO SUSPENDIDO");
        }
        return view;
    }


}
