package com.example.informatica.practicaneptuno.Adaptadors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.POJO.Categorias;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 29/03/16.
 */
public class AdaptadorCategories extends BaseAdapter {

    private List<Categorias> categorias;
    private Context context;
    LayoutInflater layoutInflater;

    public AdaptadorCategories(List<Categorias> categories, Context context) {
        this.categorias = categories;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return categorias.size();
    }

    @Override
    public Object getItem(int position) {
        return categorias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categorias.get(position).getIdCategoria();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //TODO: crear el view amb viewgroup per millor scrolling
        View view= layoutInflater.inflate(R.layout.categories_view_adapter, null);

        TextView tvCategoria=(TextView)view.findViewById(R.id.tvCategoria);
        TextView tvNombreCategoria=(TextView)view.findViewById(R.id.tvNombreCategoria);
        TextView tvDescripcion=(TextView)view.findViewById(R.id.tvDescripcion);


        tvCategoria.setText(String.valueOf(categorias.get(position).getIdCategoria()));
        tvNombreCategoria.setText(categorias.get(position).getNombreCategoria());
        tvDescripcion.setText(categorias.get(position).getDescripcion());


        return view;
    }
}