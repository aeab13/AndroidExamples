package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class Categorias implements Parcelable
{
    private int idCategoria;
    private String nombreCategoria;
    private String descripcion;
    private String imagen;

    public Categorias() {
    }

    public Categorias(int idCategoria, String nombreCategoria, String descripcion, String imagen) {
        this.idCategoria = idCategoria;
        this.nombreCategoria = nombreCategoria;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idCategoria);
        dest.writeString(nombreCategoria);
        dest.writeString(descripcion);
        dest.writeString(imagen);
    }

    public void readFromParcel (Parcel dades)
    {
        idCategoria = dades.readInt();
        nombreCategoria = dades.readString();
        descripcion = dades.readString();
        imagen = dades.readString();
    }
    public Categorias(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<Categorias> CREATOR = new Creator<Categorias>() {
        @Override
        public Categorias createFromParcel(Parcel source) {
            return new Categorias(source);
        }

        @Override
        public Categorias[] newArray(int size) {
            return new Categorias[size];
        }
    };
}
