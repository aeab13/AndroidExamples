package com.example.informatica.practicaneptuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.informatica.practicaneptuno.DAO.ProductosDAO;
import com.example.informatica.practicaneptuno.POJO.Productos;

public class NouProducte extends AppCompatActivity {
    private Button btnGuardar;
    private ProductosDAO productosDAO;
    private int idAnterior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_producte);
        productosDAO=new ProductosDAO(this);
        productosDAO.obre();
        btnGuardar= (Button) findViewById(R.id.btnGuardar);

        final EditText etNombreProducto = (EditText) findViewById(R.id.etNombreProducto);
        final EditText etSuspendido= (EditText) findViewById(R.id.etSuspendido);
        final EditText etCantidadPorUnidad = (EditText) findViewById(R.id.etCantidadPorUnidad);
        final EditText etPrecioPorUnidad = (EditText) findViewById(R.id.etPrecioPorUnidad);
        final EditText etIdProducto= (EditText) findViewById(R.id.etIdProducto);
        final EditText etIdProveedor= (EditText) findViewById(R.id.etIdProveedor);
        final EditText etIdCategoria= (EditText) findViewById(R.id.etIdCategoria);
        final EditText etUnidadesEnExistencia= (EditText) findViewById(R.id.etUnidadesEnExistencia);
        final EditText etUnidadesEnPedido= (EditText) findViewById(R.id.etUnidadesEnPedido);
        final EditText etNivelNuevoPedido= (EditText) findViewById(R.id.etNivelNuevoPedido);

        final Intent intent=getIntent();
        if(intent.getBooleanExtra(ProductesActivity.ESMODIFICAT, false))
        {
            Productos producto=intent.getParcelableExtra(ProductesActivity.MODIFICARPRODUCTE);

            if (etIdProducto != null) {
              etIdProducto.setText(String.valueOf(producto.getIdProducto()));
                idAnterior=producto.getIdProducto();
            }
            if (etIdProveedor != null) {
                etIdProveedor.setText(String.valueOf(producto.getIdProveedor()));
            }
            if (etIdCategoria != null) {
                etIdCategoria.setText(String.valueOf(producto.getIdCategoria()));
            }
            if (etUnidadesEnExistencia != null) {
                 etUnidadesEnExistencia.setText(String.valueOf(producto.getUnidadesEnExistencia()));
            }
            if (etUnidadesEnPedido != null) {
               etUnidadesEnPedido.setText(String.valueOf(producto.getUnidadesEnPedido()));
            }
            if (etNivelNuevoPedido != null) {
                  etNivelNuevoPedido.setText(String.valueOf(producto.getNivelNuevoPedido()));
            }
            if (etNombreProducto != null) {
                 etNombreProducto.setText(String.valueOf(producto.getNombreProducto()));
            }
            if (etCantidadPorUnidad != null) {
                etCantidadPorUnidad.setText(String.valueOf(producto.getCantidadPorUnidad()));
            }
            if (etPrecioPorUnidad != null) {
                 etPrecioPorUnidad.setText(String.valueOf(producto.getPrecioUnidad()));
            }
            if (etSuspendido != null) {
                 etSuspendido.setText(String.valueOf(producto.getSuspendido()));
            }
        }
        btnGuardar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(intent.getBooleanExtra(Proveidors.ESMODIFICAT, false)) {
            productosDAO.updateProducto(idAnterior, Integer.valueOf(etIdProducto.getText().toString()),etNombreProducto.getText().toString(), Integer.valueOf(etIdProveedor.getText().toString()), Integer.valueOf(etIdCategoria.getText().toString()),
                    etCantidadPorUnidad.getText().toString(), Double.valueOf(etPrecioPorUnidad.getText().toString()), Integer.valueOf(etUnidadesEnExistencia.getText().toString()), Integer.valueOf(etUnidadesEnPedido.getText().toString()),
                    Integer.valueOf(etNivelNuevoPedido.getText().toString()), Integer.valueOf(etSuspendido.getText().toString()));
          }
          else {
              productosDAO.creaProductos(Integer.valueOf(etIdProducto.getText().toString()), etNombreProducto.getText().toString(), Integer.valueOf(etIdProveedor.getText().toString()), Integer.valueOf(etIdCategoria.getText().toString()),
                      etCantidadPorUnidad.getText().toString(), Double.valueOf(etPrecioPorUnidad.getText().toString()), Integer.valueOf(etUnidadesEnExistencia.getText().toString()), Integer.valueOf(etUnidadesEnPedido.getText().toString()),
                      Integer.valueOf(etNivelNuevoPedido.getText().toString()), Integer.valueOf(etSuspendido.getText().toString()));
          }
          finish();
        }
        });

            /*private int idProducto;
        private int idProveedor;
        private int idCategoria;
        private int unidadesEnExistencia;
        private int unidadesEnPedido;
        private int nivelNuevoPedido;*/
    }
}
