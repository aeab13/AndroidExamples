package com.example.informatica.practicaneptuno.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorCategories;
import com.example.informatica.practicaneptuno.DAO.CategoriasDAO;
import com.example.informatica.practicaneptuno.POJO.Categorias;
import com.example.informatica.practicaneptuno.ProductesPerCategoria;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 28/03/16.
 */
public class CategoriesMestre extends Fragment {
    ListView lvCategories;
    CategoriasDAO categoriasDAO;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.categories_mestre, container, false);

        categoriasDAO=new CategoriasDAO(getActivity());
        categoriasDAO.obre();
        lvCategories= (ListView) view.findViewById(R.id.lvCategories);
        lvCategories.setAdapter(new AdaptadorCategories(categoriasDAO.obteCategorias(), getActivity()));
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getActivity(), ProductesPerCategoria.class);
                intent.putExtra("idCategoriaProducte", lvCategories.getItemIdAtPosition(position));
                startActivity(intent);
                Toast.makeText(getActivity(),String.valueOf(lvCategories.getItemIdAtPosition(position)), Toast.LENGTH_LONG).show();

            }
        });

        return view;

    }

}
