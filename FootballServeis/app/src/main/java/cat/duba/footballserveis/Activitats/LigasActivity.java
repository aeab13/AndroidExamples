package cat.duba.footballserveis.Activitats;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;

import cat.duba.footballserveis.Adapters.LlistaLliguesAdapter;
import cat.duba.footballserveis.FootballHttpClient;
import cat.duba.footballserveis.JSONParser.ParserJSON;
import cat.duba.footballserveis.Pojo.LlistaLligues;
import cat.duba.footballserveis.R;

public class LigasActivity extends AppCompatActivity {

    private ListView lvListaLigas;
    public static final String CODI_LLIGA="codiLliga";
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvListaLigas= (ListView) findViewById(R.id.lvListaLigas);

        new LliguesTasca().execute();
        lvListaLigas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(LigasActivity.this, SelectedLeagueActivity.class);
                intent.putExtra(CODI_LLIGA, (int)id);
                intent.putExtra("test", "399");
                startActivity(intent);
            }
        });


    }
    class LliguesTasca extends AsyncTask<String, Void, LlistaLligues>
    {
        String cadena;
        //   Meteo meteo;
        LlistaLligues llistaLligues;
        @Override
        protected LlistaLligues doInBackground(String... params) {
            //MeteoHttpClient client=new MeteoHttpClient();

            cadena=new FootballHttpClient().obtenirDadesLligues();//client.obtenirDadesMeteo("Girona");
            try {
                llistaLligues= ParserJSON.obteLlistaLligues(cadena);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return llistaLligues;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(LigasActivity.this);
            pDialog.setTitle("Please wait");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.setMessage("Loading data...");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setInverseBackgroundForced(true);
            pDialog.show();
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(LlistaLligues llistaLligues) {
            lvListaLigas.setAdapter(new LlistaLliguesAdapter(llistaLligues, LigasActivity.this));
            pDialog.dismiss();

        }
    }
}
