package cat.duba.footballserveis.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;

import cat.duba.footballserveis.Pojo.Equips;
import cat.duba.footballserveis.R;
import cat.duba.footballserveis.SVGStuff.SvgDecoder;
import cat.duba.footballserveis.SVGStuff.SvgDrawableTranscoder;
import cat.duba.footballserveis.SVGStuff.SvgSoftwareLayerSetter;

/**
 * Created by abde on 20/04/16.
 */
public class EquipsLligaAdapter extends BaseAdapter {
    Equips equips;
    LayoutInflater layoutInflater;
    Context context;

    public EquipsLligaAdapter(Equips equips, Context context) {
        this.equips = equips;
        layoutInflater=LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public int getCount() {
        return equips.getEquips().size();
    }

    @Override
    public Object getItem(int position) {
        return equips.getEquips().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if(vi==null)
        {
            vi=layoutInflater.inflate(R.layout.equips_lliga_adapter,parent, false);
            holder=new ViewHolder();
            holder.ivLogo= (ImageView) vi.findViewById(R.id.ivLogo);
            holder.tvNomEquip= (TextView) vi.findViewById(R.id.tvNomEquip);
            holder.tvNomCurt= (TextView) vi.findViewById(R.id.tvNomCurt);
            holder.tvValorEquip= (TextView) vi.findViewById(R.id.tvValorEquip);

            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();

        }
        Equips.Equip item = (Equips.Equip) getItem(position);


        if(item.getCrestUrl().substring(item.getCrestUrl().length()-3).equals("svg"))
        {
            GenericRequestBuilder requestBuilder = Glide.with(context)
                    .using(Glide.buildStreamModelLoader(Uri.class, context), InputStream.class)
                    .from(Uri.class)
                    .as(SVG.class)
                    .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                    .sourceEncoder(new StreamEncoder())
                    .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                    .decoder(new SvgDecoder())
                    .animate(android.R.anim.fade_in)
                    .listener(new SvgSoftwareLayerSetter<Uri>());
            Uri uri = Uri.parse(item.getCrestUrl());
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            // SVG cannot be serialized so it's not worth to cache it
                    .load(uri)
                    .into(holder.ivLogo);
        }
        else{
            new DownloadImageTask(holder.ivLogo)
                    .execute(item.getCrestUrl());
        }

        if(item.getCode()!=null)
            holder.tvNomEquip.setText(item.getName()+" ("+item.getCode()+")");
        else
            holder.tvNomEquip.setText(item.getName());
        if(item.getShortName()!=null)
            holder.tvNomCurt.setText(item.getShortName());
        holder.tvValorEquip.setText("Valor del equipo: "+item.getSquadMarketValue());

        return vi;
    }
    static class ViewHolder{
        ImageView ivLogo;
        TextView tvNomEquip, tvNomCurt, tvValorEquip;
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
