package cat.duba.footballserveis.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abde on 22/04/16.
 */
public class Jornades{
    ArrayList<Jornada> jornada=new ArrayList<>();

    public ArrayList<Jornada> getJornada() {
        return jornada;
    }

    public void setJornada(ArrayList<Jornada> jornada) {
        this.jornada = jornada;
    }

    public Jornades()
    {

    }
    public static class Jornada
    {
        String date;
        String status;
        int matchday;
        String homeTeamName;
        String awayTeamName;
        int goalsHomeTeam;
        int goalsAwayTeam;
        public Jornada()
        {}
        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getMatchday() {
            return matchday;
        }

        public void setMatchday(int matchday) {
            this.matchday = matchday;
        }

        public String getHomeTeamName() {
            return homeTeamName;
        }

        public void setHomeTeamName(String homeTeamName) {
            this.homeTeamName = homeTeamName;
        }

        public String getAwayTeamName() {
            return awayTeamName;
        }

        public void setAwayTeamName(String awayTeamName) {
            this.awayTeamName = awayTeamName;
        }

        public int getGoalsHomeTeam() {
            return goalsHomeTeam;
        }

        public void setGoalsHomeTeam(int goalsHomeTeam) {
            this.goalsHomeTeam = goalsHomeTeam;
        }

        public int getGoalsAwayTeam() {
            return goalsAwayTeam;
        }

        public void setGoalsAwayTeam(int goalsAwayTeam) {
            this.goalsAwayTeam = goalsAwayTeam;
        }

        protected Jornada(Parcel in) {
            date = in.readString();
            status = in.readString();
            matchday = in.readInt();
            homeTeamName = in.readString();
            awayTeamName = in.readString();
            goalsHomeTeam = in.readInt();
            goalsAwayTeam = in.readInt();
        }
    }

}
