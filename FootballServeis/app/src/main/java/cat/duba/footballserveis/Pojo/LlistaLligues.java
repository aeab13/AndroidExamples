package cat.duba.footballserveis.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abde on 19/04/16.
 */
public class LlistaLligues implements Parcelable{
    private ArrayList<Lligues> lliguesArrayList=new ArrayList<Lligues>();
    public ArrayList<Lligues> getLliguesArrayList() {
        return lliguesArrayList;
    }

    public LlistaLligues()
    {

    }

    public void setLliguesArrayList(ArrayList<Lligues> lliguesArrayList) {
        this.lliguesArrayList = lliguesArrayList;
    }
    public static class Lligues implements Parcelable {
        private int id;
        private int currentMatchday;
        private int numberOfMatchdays;
        private int numberOfTeams;
        private int numberOfGames;
        private String caption;
        private String league;
        private String lastUpdated;
        private String self;
        private String teams;
        private String fixtures;
        private String leagueTable;
        private String year;

        public Lligues()
        {}

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public int getCurrentMatchday() {
            return currentMatchday;
        }

        public void setCurrentMatchday(int currentMatchday) {
            this.currentMatchday = currentMatchday;
        }

        public int getNumberOfMatchdays() {
            return numberOfMatchdays;
        }

        public void setNumberOfMatchdays(int numberOfMatchdays) {
            this.numberOfMatchdays = numberOfMatchdays;
        }

        public int getNumberOfTeams() {
            return numberOfTeams;
        }

        public void setNumberOfTeams(int numberOfTeams) {
            this.numberOfTeams = numberOfTeams;
        }

        public int getNumberOfGames() {
            return numberOfGames;
        }

        public void setNumberOfGames(int numberOfGames) {
            this.numberOfGames = numberOfGames;
        }

        public String getCaption() {
            return caption;
        }

        public void setCaption(String caption) {
            this.caption = caption;
        }

        public String getLeague() {
            return league;
        }

        public void setLeague(String league) {
            this.league = league;
        }

        public String getLastUpdated() {
            return lastUpdated;
        }

        public void setLastUpdated(String lastUpdated) {
            this.lastUpdated = lastUpdated;
        }

        public String getSelf() {
            return self;
        }

        public void setSelf(String self) {
            this.self = self;
        }

        public String getTeams() {
            return teams;
        }

        public void setTeams(String teams) {
            this.teams = teams;
        }

        public String getFixtures() {
            return fixtures;
        }

        public void setFixtures(String fixtures) {
            this.fixtures = fixtures;
        }

        public String getLeagueTable() {
            return leagueTable;
        }

        public void setLeagueTable(String leagueTable) {
            this.leagueTable = leagueTable;
        }

        protected Lligues(Parcel in) {
            id = in.readInt();
            currentMatchday = in.readInt();
            numberOfMatchdays = in.readInt();
            numberOfTeams = in.readInt();
            numberOfGames = in.readInt();
            caption = in.readString();
            league = in.readString();
            lastUpdated = in.readString();
            self = in.readString();
            teams = in.readString();
            fixtures = in.readString();
            leagueTable = in.readString();
            year = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(currentMatchday);
            dest.writeInt(numberOfMatchdays);
            dest.writeInt(numberOfTeams);
            dest.writeInt(numberOfGames);
            dest.writeString(caption);
            dest.writeString(league);
            dest.writeString(lastUpdated);
            dest.writeString(self);
            dest.writeString(teams);
            dest.writeString(fixtures);
            dest.writeString(leagueTable);
            dest.writeString(year);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Lligues> CREATOR = new Parcelable.Creator<Lligues>() {
            @Override
            public Lligues createFromParcel(Parcel in) {
                return new Lligues(in);
            }

            @Override
            public Lligues[] newArray(int size) {
                return new Lligues[size];
            }
        };
    }

    protected LlistaLligues(Parcel in) {
        lliguesArrayList= (ArrayList<Lligues>) in.readSerializable();
    }

    public static final Creator<LlistaLligues> CREATOR = new Creator<LlistaLligues>() {
        @Override
        public LlistaLligues createFromParcel(Parcel in) {
            return new LlistaLligues(in);
        }

        @Override
        public LlistaLligues[] newArray(int size) {
            return new LlistaLligues[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeArray(lliguesArrayList.toArray());
    }

}
