package cat.duba.footballserveis;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import cat.duba.footballserveis.Fragment.ClassificacioFragment;
import cat.duba.footballserveis.Fragment.EquipsFragment;
import cat.duba.footballserveis.Fragment.PartitsFragment;

/**
 * Created by abde on 19/04/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    public ViewPagerAdapter(FragmentManager fm,int i) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        // In my case we are showing up only one fragment in all the three tabs so we are
        switch (position)
        {
            case 0:
                return new ClassificacioFragment();
            case 1:
                return new EquipsFragment();
            case 2:
                return new PartitsFragment();
        }
            // Which Fragment should be dislpayed by the viewpager for the given position
        // not worrying about the position and just returning the TabFragment
        return null;
    }

    @Override
    public int getCount() {
        return 3;           // As there are only 3 Tabs
    }

}
