package cat.duba.footballserveis.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abde on 21/04/16.
 */
public class Equips implements Parcelable {
    String self;
    String soccerseason;
    int count;
    ArrayList<Equip> equips=new ArrayList<>();
    public Equips()
    {

    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getSoccerseason() {
        return soccerseason;
    }

    public void setSoccerseason(String soccerseason) {
        this.soccerseason = soccerseason;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Equip> getEquips() {
        return equips;
    }

    public void setEquips(ArrayList<Equip> equips) {
        this.equips = equips;
    }

    protected Equips(Parcel in) {
        self = in.readString();
        soccerseason = in.readString();
        count = in.readInt();
        equips = in.createTypedArrayList(Equip.CREATOR);
    }

    public static final Creator<Equips> CREATOR = new Creator<Equips>() {
        @Override
        public Equips createFromParcel(Parcel in) {
            return new Equips(in);
        }

        @Override
        public Equips[] newArray(int size) {
            return new Equips[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(self);
        parcel.writeString(soccerseason);
        parcel.writeInt(count);
        parcel.writeTypedList(equips);
    }

    public static class Equip implements Parcelable
    {
        String self;
        String fixtures;
        String players;
        String name;
        String code;
        String shortName;
        String squadMarketValue;
        String crestUrl;

        public Equip()
        {

        }
        protected Equip(Parcel in) {
            self = in.readString();
            fixtures = in.readString();
            players = in.readString();
            name = in.readString();
            code = in.readString();
            shortName = in.readString();
            squadMarketValue = in.readString();
            crestUrl = in.readString();
        }

        public static final Creator<Equip> CREATOR = new Creator<Equip>() {
            @Override
            public Equip createFromParcel(Parcel in) {
                return new Equip(in);
            }

            @Override
            public Equip[] newArray(int size) {
                return new Equip[size];
            }
        };

        public String getSelf() {
            return self;
        }

        public void setSelf(String self) {
            this.self = self;
        }

        public String getFixtures() {
            return fixtures;
        }

        public void setFixtures(String fixtures) {
            this.fixtures = fixtures;
        }

        public String getPlayers() {
            return players;
        }

        public void setPlayers(String players) {
            this.players = players;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getShortName() {
            return shortName;
        }

        public void setShortName(String shortName) {
            this.shortName = shortName;
        }

        public String getSquadMarketValue() {
            return squadMarketValue;
        }

        public void setSquadMarketValue(String squadMarketValue) {
            this.squadMarketValue = squadMarketValue;
        }

        public String getCrestUrl() {
            return crestUrl;
        }

        public void setCrestUrl(String crestUrl) {
            this.crestUrl = crestUrl;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(self);
            parcel.writeString(fixtures);
            parcel.writeString(players);
            parcel.writeString(name);
            parcel.writeString(code);
            parcel.writeString(shortName);
            parcel.writeString(squadMarketValue);
            parcel.writeString(crestUrl);
        }
    }
}
