package cat.duba.footballserveis.Fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONException;

import cat.duba.footballserveis.Adapters.ClassificacioAdapter;
import cat.duba.footballserveis.FootballHttpClient;
import cat.duba.footballserveis.JSONParser.ParserJSON;
import cat.duba.footballserveis.Pojo.Classificacions;
import cat.duba.footballserveis.R;

/**
 * Created by abde on 19/04/16.
 */
public class ClassificacioFragment extends Fragment {
    ListView lvClassi;
    int idLliga;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_classificacio, container, false);
        lvClassi= (ListView) v.findViewById(R.id.lvClassi);
        idLliga=(Integer) container.getTag(R.string.SELECTED_LEAGUE_ID);
        new ClassificacioTasca().execute();
        return v;
    }

    class ClassificacioTasca extends AsyncTask<String, Void, Classificacions>
    {
        Classificacions classi;
        @Override
        protected Classificacions doInBackground(String... params) {
            FootballHttpClient connexio=new FootballHttpClient();
            try {
                classi= ParserJSON.obteLlistaClassificacions(connexio.obtenirClassificacio(idLliga));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return classi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Classificacions classi) {
            lvClassi.setAdapter(new ClassificacioAdapter(classi, ClassificacioFragment.this.getContext()));

        }
    }
}
