package cat.duba.puzzlenou;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    GridLayout gdPuzzle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gdPuzzle= (GridLayout) findViewById(R.id.gdPuzzle);
        TextView tv=new TextView(this);
        tv.setBackgroundColor(Color.parseColor("#fffff"));
        GridLayout.Spec fila1 = GridLayout.spec(0, 2);

        GridLayout.Spec col1 = GridLayout.spec(0); //Assignem la tercera columna amb una amplada de 1 (valor per defecte del ColSpan)
        //Assignem a unes coordenades
        tv.setLayoutParams(new GridLayout.LayoutParams(fila1,col1));
    }
}
