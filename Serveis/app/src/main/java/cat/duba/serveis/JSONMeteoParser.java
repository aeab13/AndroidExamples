package cat.duba.serveis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abde on 12/04/16.
 */
public class JSONMeteoParser {
    public static Meteo obteMeteo(String dades) throws JSONException {
        Meteo meteo=new Meteo();
        JSONObject jObj=new JSONObject(dades);

        JSONObject coordenada=jObj.getJSONObject("coord");
        meteo.getCoordenada().setLongitut(coordenada.getDouble("lon"));
        meteo.getCoordenada().setLatitud(coordenada.getDouble("lat"));

        JSONArray jsonArray=jObj.getJSONArray("weather");
        JSONObject jTemps=jsonArray.getJSONObject(0);
        meteo.getTemps().get(0).setId(jTemps.getInt("id"));
        meteo.getTemps().get(0).setMain(jTemps.getString("main"));
        meteo.getTemps().get(0).setDescripcio(jTemps.getString("description"));
        meteo.getTemps().get(0).setIcona(jTemps.getString("icon"));

        meteo.setBase(jObj.getString("base"));

        JSONObject main=jObj.getJSONObject("main");
        meteo.getPrincipal().setTemperatura(main.getDouble("temp"));
        meteo.getPrincipal().setPressio(main.getDouble("pressure"));
        meteo.getPrincipal().setHumitat(main.getDouble("humidity"));
        meteo.getPrincipal().setTemperaturaMaxima(main.getDouble("temp_max"));
        meteo.getPrincipal().setTemperaturaMinima(main.getDouble("temp_min"));

        JSONObject vent=jObj.getJSONObject("wind");
        meteo.getVent().setVelocitat(vent.getDouble("speed"));
        meteo.getVent().setGraus(vent.getDouble("deg"));

        JSONObject nuvols=jObj.getJSONObject("clouds");
        meteo.getNuvols().setTot(nuvols.getInt("all"));

        meteo.setData(jObj.getLong("dt"));

        JSONObject sys=jObj.getJSONObject("sys");
        meteo.getSistema().setTipus(sys.getInt("type"));
        meteo.getSistema().setId(sys.getInt("message"));
        meteo.getSistema().setMissatge(sys.getDouble("message"));
        meteo.getSistema().setPais(sys.getString("country"));
        meteo.getSistema().setAlba(sys.getLong("sunrise"));
        meteo.getSistema().setOcas(sys.getLong("sunset"));

        meteo.setId(jObj.getLong("id"));
        meteo.setNom(jObj.getString("name"));
        meteo.setCode(jObj.getInt("cod"));

        return meteo;


        //Meteo.Temps mTemps=new Meteo.Temps();

    }

}
