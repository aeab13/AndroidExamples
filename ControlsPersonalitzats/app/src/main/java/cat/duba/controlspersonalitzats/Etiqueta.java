package cat.duba.controlspersonalitzats;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by abde on 7/03/16.
 */
public class Etiqueta extends TextView {

    private final int GRUIX_MARC=2;
    private int ColorFonsSelecionat, colorMarc, colorLletraSeleccionat;
    private boolean seleccionat=false;


    private int colorTextOriginal;
    private Drawable fonsOriginal;


    public Etiqueta(Context context)
    {
        super(context);
        Inicialitzacio(context, null);
    }

    public Etiqueta(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        Inicialitzacio(context, attrs);
    }

    public Etiqueta(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        Inicialitzacio(context, attrs);
    }


    private void Inicialitzacio(Context context, AttributeSet attrs)
    {

    }
}
